<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/**
 * Api Part
 */
Route::group(['prefix' => 'v1'], function(){


    Route::group(['middleware' => 'customapi'], function(){

        Route::get('/', function(){return 'Success';});
        Route::get('/all_authors', 'ApiController@allAurhorsAPI');
        Route::get('/author_page/{authors}', 'ApiController@show_author_pageAPI');
        Route::get('/home_page/{slideshow_id}','ApiController@slideshowAPI');
        Route::get('/new_added_books','ApiController@new_added_booksAPI');
        Route::get('/most_listened_books','ApiController@most_listened_booksAPI');
        Route::get('/top_rated_books','ApiController@top_rated_booksAPI');
        Route::post('/login','ApiController@loginAPI');
        Route::post('/forgot_password','ApiController@forgotPasswordAPI');
        Route::get('/all_books','ApiController@allBooksAPI');
        Route::get('/all_books/{author_id}','ApiController@AuthorBooks');
        Route::get('audio/{book_record_title}/{user_id}', 'ApiController@BookRecord');
        Route::get('/book','ApiController@showBookAPI');
        Route::get('/purchased_books/{user_id}','ApiController@UserPurchasedBooksAPI');
        Route::get('/favorite_books/{user_id}','ApiController@UserFavoriteBooksAPI');


        Route::post('/favorite_books_delete','ApiController@UserFavoriteBooksDestroy');
        Route::post('/book_wishlist','ApiController@AddWishList');
        Route::get('/page/{name}','ApiController@PageContentAPI');

        Route::post('/search_author','ApiController@SearchAuthor');
        Route::post('/search_book','ApiController@SearchBook');


        Route::get('/register','ApiController@getRegistration');
        Route::post('/register','ApiController@postRegistration');

        Route::post('password/email', 'Auth\ForgotPasswordController@getResetToken');
        Route::post('password/reset', 'Auth\ResetPasswordController@resetApi');


    });

});

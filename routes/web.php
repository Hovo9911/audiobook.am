<?php

Route::get('/slider', function () {return view('slider');});
/*
Route::get('/test', function () {  return view('test');});
Route::get('/test2', function () { return view('test2');});
Route::get('/book_page2', function () { return view('book_page2');});
Route::get('/', function () { return view('home');});
Route::get('/books', function () { return view('books');});
*/

/**
 *  Home page
 */
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

/**
 * Auth part
 */
Auth::routes();

/**
 * Social Auth
 */

Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');


/**
 * Pages
 */
Route::get('/faq', function () {return view('faq');});
Route::get('/contact', function () {return view('pages/contact');});

Route::get('/about', function () {
    $data = \App\Pages::where(['sku'=>'about'])->first();
    return view('pages/about',compact('data'));
});

Route::get('/terms_of_use', function () {
    $data = \App\Pages::where(['sku'=>'terms_of_use'])->first();
    return view('pages/terms_of_use',compact('data'));
});

Route::get('/privacy_policy', function () {
    $data = \App\Pages::where(['sku'=>'privacy_policy'])->first();
    return view('pages/privacy_policy',compact('data'));
});

/**
 * Exchange
 */
Route::get('cb/exchange', 'ExchangeController@GetCbExchange');


/**
 *  Contact
 */
Route::post('contact', 'UserController@ContactUs');


/**
 * Books Part
 */
Route::get('/book_page', function () {   return view('book_page');});
Route::get('/rated_books', function () { return view('books/rated_books');});

Route::post('book/rate', 'BookCategoriesController@BookRate');
Route::get('/books', 'BookCategoriesController@index');
Route::get('/book_page/{book_name}', 'BookCategoriesController@show_book_page');
Route::get('/top_rated_books','BookCategoriesController@top_rated_books');
Route::get('/favourite_books','BookCategoriesController@favourite_books');
Route::get('/new_added_books','BookCategoriesController@new_added_books');
Route::get('/most_listened_books','BookCategoriesController@most_listened_books');

/**
 * Authors Part
 */
Route::get('/all_authors', 'AuthorController@index');
Route::get('/lovely_authors', 'AuthorController@LovelyAuthors');
Route::get('/authors/checkAuthorAndBookName','AuthorController@checkAuthorAndBookName');
Route::get('/authors/checkAuthorName','AuthorController@checkAuthorName');
Route::get('/author_page/{authors}', 'AuthorController@show_author_page');


/**
 * Confirmation
 */
Route::post('/confirmation','ConfirmationController@index');


/**
 * Routes for Auth Users
 */
Route::group(['middleware' => 'auth'], function() {

    Route::get('confirmation/{id}','ConfirmationController@index');
    Route::post('book/add-wish-list','UserController@AddWishList');
    Route::delete('book/remove-wish-list/{id}','UserController@DestroyWishList');
    Route::get('personal_page','UserController@PersonalPage');
    Route::put('user/personal_info','UserController@PersonalInfoUpdate');

    /**
     * Payment PayPal
     */
    Route::group(['namespace' => 'PayPal'], function () {
//        Route::get('paypal', 'PayPalController@PayPalManualPayment');
        Route::get('paypal/confirm/{id}', 'PayPalController@PayPalManualConfirm');

        Route::get('payment/success', function () {return view('paypal.success');});
        Route::get('payment/cancel', function () {return view('paypal.cancel');});

    });
    /**
     * Payment Ameria
     */
    Route::group(['namespace' => 'Ameria'], function () {
        Route::post('/confirm_ameria_payment','AmeriaController@AmeriaGetPaymentFields');
    });

    /**
     * Checkout Part
     */
    Route::post('checkout', 'CheckoutController@store');
});


Route::group(['middleware' => 'revalidate'], function()
{
    Auth::routes();
    Route::get('/home', 'HomeController@index');
});




/**
 * Admin
 */

Route::group(['middleware' => 'admin','prefix'=>'admin','namespace'=>'Admin'], function() {
    Route::resource('book','BookController');
});




//Route::get('audio/{book}', function($href) {
//
////  dd($book_id);
//    $book_id = App\BookRecords::where(['book_record_title'=>$href])->pluck('book_id')->first();
//
//    if(!is_null(\App\BookBilling::where(['book_id'=>$book_id,'user_id'=>\Illuminate\Support\Facades\Auth::id()])->first())){
//
//        $book_record = \App\BookRecords::where(['book_id'=>$book_id])->pluck('book_record')->first();
//
//        $path = storage_path() . '/uploads/tmp/'.$book_record.'.mp3';
//
//        $file = \Illuminate\Support\Facades\File::get($path);
//
//
//        $type = \Illuminate\Support\Facades\File::mimeType($path);
//
//        $response = \Illuminate\Support\Facades\Response::make($file, 200);
//        $response->header("Content-Type", $type);
//
//
//
//        return $response;
//
//    }
//
//})->where([
//    'file' => '[a-zA-Z0-9-_]+', // the file name (no dots)
//    'extension' => '\..+' // include the dot as the first character of extension
//]);


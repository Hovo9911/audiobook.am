@extends('layouts.app')

@section('content')

<div class="container-fluid privacy_policy">
<div class="inside_content">
<p class="under_title" style="font-size: 16px; max-width: 300px;"><b>{{$data->title}}</b></p><br><br>
<p>{!!nl2br(e($data->content))!!}</p><br><br>
<p style="text-align:right"><i>Վերջին թարմացումը՝ 16.02.2018</i></p>
</div>    
</div>

@endsection
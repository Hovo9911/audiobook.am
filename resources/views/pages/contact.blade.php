@extends('layouts.app')

@section('content')



    <section class="page_content">
        <div class="container contact_content">

            <div class="form">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="under_title" style="line-height:25px;max-width: 180px"><b>Հետադարձ կապ</b></h4>
                <form style="margin-top:50px;" method="post" action="{{url('contact')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Անուն" oninvalid="InvalidMsg(this);" required>
                            <input type="text" name="subject" value="{{ old('subject') }}"  class="form-control" style="margin-top:50px;" placeholder="Թեմա">
                        </div>
                        <div class="col-sm-6">
                            <input type="email" name="email" value="{{ old('email') }}"  class="form-control" placeholder="Էլ.փոստ" oninput="InvalidMsg(this);" oninvalid="InvalidMsg(this);" required>
                        </div>
                    </div>
                    <div class="row" style="margin-top:35px;">
                        <div class="form-group">
                            <label for="comment">Հաղորդագրություն</label>
                            <textarea name="message" class="form-control" rows="10" id="comment" oninvalid="InvalidMsg(this);" required>{{ old('message') }}</textarea>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-xs-6">
                            <div class="fileUpload btn">
                                <span>Ընտրել ֆայլ</span>
                                <input id="uploadBtn" name="file" type="file" class="upload"/>
                            </div>
                            <input id="uploadFile" placeholder="Ֆայլ ընտրված չէ" disabled="disabled"/>
                        </div>
                        <div class="col-xs-6">
                            <div class="g-recaptcha" data-sitekey="6LdGFDwUAAAAAIHqTTxxov1LDQzp4pYJZK0OG3Xz"></div>
                        </div>
                    </div>


                    <button type="submit" class="btn btn-contact"><span class="glyphicon glyphicon-send"></span>
                    </button>
                </form>
            </div>
        </div>
    </section>

    {{--<div class="share42init" data-url="http://audiobook.am/contact" data-image="https://i.ytimg.com/vi/_MV_cpZi7ng/maxresdefault.jpg" data-title="test" data-description="aaa" ></div>--}}
    {{--<script type="text/javascript" src="{{asset('plugins/share42/share42.js')}}"></script>--}}

    {{--<span id="button_1"></span>--}}

    {{--<a href="https://www.facebook.com/sharer/sharer.php?u=http://audiobook.am/contact"--}}
       {{--target="_blank">--}}
        {{--Share on Facebook--}}
    {{--</a>--}}

@section('scripts')

    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">
        stLight.options({
            publisher:'12345',
        });
    </script>
    <script src='https://www.google.com/recaptcha/api.js?explicit&hl=hy'></script>

    <script>
        stWidget.addEntry({
            "service":"sharethis",
            "element":document.getElementById('button_1'),
            "url":"http://audiobook.am/contact",
            "title":"shun",
            "type":"large",
            "text":"katu" ,
            "description":"katu" ,
            "image":"http://audiobook.am/images/books/Mtnadzor.png",
            "summary":"this is cccdescription1"
        });
    </script>
@endsection

@endsection
@extends('layouts.app')

@section('content')
<section class="page_content" style="min-height:600px;">
<div class="container" style="position:relative;min-height:600px;">
<img src="images/about_img.jpg" class="about_img">
   <div class="about">
     <h1 style="color:#ff8533;text-align:right;font-size: 46px;">{{$data->title}} </h1>
     <h1 style="    padding-left: 20px;color:#ff8533;font-size: 50px;text-align:right;">{{$data->content_title}} </h1>
     <hr style="border-top: 5px solid #ff8533;">   
     <p style="text-align:justify;margin-top:30px;">{!!$data->content  !!}}</p>
       
   </div>
</div>
</section>

@endsection
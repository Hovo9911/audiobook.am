@extends('layouts.app')

@section('content')
<style>
/* faq css  */
.faq_content a:focus,
.faq_content a:hover,
.faq_content a:active {
  outline: 0;
  text-decoration: none;
}

.faq_content .panel {
  border-width: 0 0 1px 0;
  border-style: solid;
  border-color: #fff;
  background: none;
  box-shadow: none;
}

.faq_content .panel:last-child {
  border-bottom: none;
}

.faq_content .panel-group > .panel:first-child .panel-heading {
  border-radius: 4px 4px 0 0;
}

.faq_content .panel-group .panel {
  border-radius: 0;
}

.faq_content .panel-group .panel + .panel {
  margin-top: 0;
}

.faq_content .panel-heading {
  border-radius: 0;
  border: none;
  color: #fff;
  padding: 0;
  border-bottom: 1px solid #a6a6a6;
}

.faq_content .panel-title a {
  display: block;
  color: #000000;
  padding: 15px;
  position: relative;
  font-size: 16px;
  font-weight: 400;
}

.faq_content .panel-body {
  background: #fff;
}

.faq_content .panel:last-child .panel-body {
  border-radius: 0 0 4px 4px;
}

.faq_content .panel:last-child .panel-heading {
  transition: border-radius 0.3s linear 0.2s;
}

.faq_content .panel:last-child .panel-heading.active {
  border-radius: 0;
  transition: border-radius linear 0s;
}
/* #bs-collapse icon scale option */

.faq_content .panel-heading a:before {
  content: '\e146';
  position: absolute;
  font-family: 'Material Icons';
  right: 5px;
  top: 10px;
  font-size: 24px;
  transition: all 0.5s;
  transform: scale(1);
  color: #FFA500;
}

.faq_content .panel-heading.active a:before {
  content: ' ';
  transition: all 0.5s;
  transform: scale(0);
}

#bs-collapse .panel-heading a:after {
  content: ' ';
  font-size: 24px;
  position: absolute;
  font-family: 'Material Icons';
  right: 5px;
  top: 10px;
  transform: scale(0);
  transition: all 0.5s;
}

#bs-collapse .panel-heading.active a:after {
  content: '\e909';
  transform: scale(1);
  transition: all 0.5s;  
  color: #FFA500;
}
.faq_nav>li.active>a{
    border-bottom:none;
}
.faq_tab{
    background: #fff;
}
.faq_content .tab-content > .active{
    padding: 30px 60px; 
    min-height: 600px;
    box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.2);
}
.faq_nav>li>a {
    padding: 7px 10px;
}    
/* and faq css  */
</style>
<div class="container-fluid faq_content">
<div class="row">  
<div class="col-md-3" style="background:#fff;padding:30px;box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);">
<p class="under_title" style="font-size:16px;max-width: 200px"><b>Հաճախ Տրվող Հարցեր</b></p><br>
<ul class="nav nav-pills nav-stacked faq_nav">
  <li class="active"><a href="#tab_a" data-toggle="pill">Ընդհանուր </a></li>
  <li><a href="#tab_b" data-toggle="pill">Անվտանգություն</a></li>
  <li><a href="#tab_c" data-toggle="pill">Ծառայություններ</a></li>
  <li><a href="#tab_d" data-toggle="pill">Բիլինգ</a></li>
</ul>
</div>  
<div class="tab-content col-md-9">
    <div class="tab-pane active faq_tab" id="tab_a">
        <p class="under_title" style="font-size:16px;max-width: 200px"><b>Ընդհանուր</b></p><br>
        <div class="panel-group wrap" id="bs-collapse1">

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse1" href="#one">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="one" class="panel-collapse collapse in">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris, sit vestibulum amet accumsan eros. Integer commodo exercitationem. Dis wisi mi nisl ut erat, pellentesque massa imperdiet fusce, malesuada neque proin laoreet magna ut conubia, potenti dui tempor tempor, leo arcu enim donec. Tempor mauris tincidunt vestibulum luctus purus, condimentum nibh nunc ut pellentesque, risus ut tortor, a morbi hendrerit a massa. Eget vitae magna aliquam arcu justo.
          </div>
        </div>

      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse1" href="#two">
         Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="two" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>

        </div>
      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse1" href="#three">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="three" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>
        </div>
      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse1" href="#four">
         Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="four" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>
        </div>
      </div>
      <!-- end of panel -->

    </div>
    </div>
    <div class="tab-pane faq_tab" id="tab_b">
        <p class="under_title" style="font-size:16px;max-width: 200px"><b>Անվտանգություն</b></p><br>
        <div class="panel-group wrap" id="bs-collapse2">
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse2" href="#onet">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="onet" class="panel-collapse collapse in">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris, sit vestibulum amet accumsan eros. Integer commodo exercitationem. Dis wisi mi nisl ut erat, pellentesque massa imperdiet fusce, malesuada neque proin laoreet magna ut conubia, potenti dui tempor tempor, leo arcu enim donec. Tempor mauris tincidunt vestibulum luctus purus, condimentum nibh nunc ut pellentesque, risus ut tortor, a morbi hendrerit a massa. Eget vitae magna aliquam arcu justo.
          </div>
        </div>

      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse2" href="#twot">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="twot" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>

        </div>
      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse2" href="#threet">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="threet" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>
        </div>
      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse2" href="#fourt">
         Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="fourt" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>
        </div>
      </div>
      <!-- end of panel -->

    </div>
    </div>
    <div class="tab-pane faq_tab" id="tab_c">
        <p class="under_title" style="font-size:16px;max-width: 200px"><b>Ծառայություններ</b></p><br>
                <div class="panel-group wrap" id="bs-collapse3">
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse3" href="#onek">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="onek" class="panel-collapse collapse in">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris, sit vestibulum amet accumsan eros. Integer commodo exercitationem. Dis wisi mi nisl ut erat, pellentesque massa imperdiet fusce, malesuada neque proin laoreet magna ut conubia, potenti dui tempor tempor, leo arcu enim donec. Tempor mauris tincidunt vestibulum luctus purus, condimentum nibh nunc ut pellentesque, risus ut tortor, a morbi hendrerit a massa. Eget vitae magna aliquam arcu justo.
          </div>
        </div>

      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse3" href="#twok">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="twok" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>

        </div>
      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse3" href="#threek">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="threek" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>
        </div>
      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse3" href="#fourk">
         Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="fourk" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>
        </div>
      </div>
      <!-- end of panel -->

    </div>
    </div>
    <div class="tab-pane faq_tab" id="tab_d">
        <p class="under_title" style="font-size:16px;max-width: 200px"><b>Բիլինգ</b></p><br>
                <div class="panel-group wrap" id="bs-collapse4">
      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse4" href="#oneg">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="oneg" class="panel-collapse collapse in">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris, sit vestibulum amet accumsan eros. Integer commodo exercitationem. Dis wisi mi nisl ut erat, pellentesque massa imperdiet fusce, malesuada neque proin laoreet magna ut conubia, potenti dui tempor tempor, leo arcu enim donec. Tempor mauris tincidunt vestibulum luctus purus, condimentum nibh nunc ut pellentesque, risus ut tortor, a morbi hendrerit a massa. Eget vitae magna aliquam arcu justo.
          </div>
        </div>

      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse4" href="#twog">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="twog" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>

        </div>
      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse4" href="#threeg">
          Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="threeg" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>
        </div>
      </div>
      <!-- end of panel -->

      <div class="panel">
        <div class="panel-heading">
          <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#bs-collapse4" href="#fourg">
         Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales ?
        </a>
      </h4>
        </div>
        <div id="fourg" class="panel-collapse collapse">
          <div class="panel-body">
            Lorem ipsum dolor sit amet, eleifend interdum vulputate mauris lorem sodales, nullam in ac et at dictumst neque, fusce est, turpis montes sed quis gravida, et massa facilisis ut ac at libero. Ultrices vitae lectus, consectetuer nullam, ullamcorper ipsum in eu risus convallis eget. Magna diam risus in nulla, nisl mauris arcu dui id at non. Eu lectus leo, molestie id wisi congue placerat neque in, vulputate libero maecenas dui mauris
          </div>
        </div>
      </div>
      <!-- end of panel -->

    </div>
    </div>
</div><!-- tab content -->
</div>      
</div>
<script>
  $(document).ready(function() {
  $('.collapse.in').prev('.panel-heading').addClass('active');
  $('#bs-collapse')
    .on('show.bs.collapse', function(a) {
      $(a.target).prev('.panel-heading').addClass('active');
    })
    .on('hide.bs.collapse', function(a) {
      $(a.target).prev('.panel-heading').removeClass('active');
    });
});  
</script>
@endsection


@if(count($books) > 0)
@foreach($books as $book)
    <div class="col-sm-4">
        <div class="books">
            <img src="{{ $book->book_image }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title">
                    <b>{{$book->book_title }}</b>
                </p>
                @if($book->author_id)
                <p class="book_author">Հեղինակ` <a
                            href="{{url('author_page/'.$book->href)}}"> {{ $book->author_name}}</a></p>
                {{--<img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">   --}}
               @endif
                <div class="rateBookAveRage" data-rateyo-rating="{{$book->book_average_rating}}"></div>
            </div>
            <a href="{{ url('/book_page/'.$book->book_href) }}">
                <button type="button" class="btn btn-default">Լսել Գիրքը</button>
            </a>
        </div>
    </div>
@endforeach
<div class="clearfix col-sm-12">
<nav style="text-align: center">
    {{$books->links()}}
</nav>
    @else
<p class="no-more">Որոնման չափորոշիչներին համապատասխան արդյունք չի գտնվել:</p>

@endif
</div>

<div class="row" style="margin-bottom:30px;">
    <h4><b style="border-bottom:3px solid #ff6600;padding-bottom:5px;">Ձեզ կարող է հետաքրքրել նաև</b></h4><hr> 
        <div class="center" style="max-width:762px;margin:0 auto;">
            @foreach($slideshow_may_interested_books as $slideshow_may_interested_book)
            <div class="books">
                <img src="{{ $slideshow_may_interested_book->book_image }}" class="img-responsive book_img" alt="img">
                    <div class="books_info">
                        <p class="book_title"><b>{{ $slideshow_may_interested_book->book_title }}</b></p>
                        <p class="book_author">Հեղինակ ` {{ $slideshow_may_interested_book->author_name}}</p>
                        <div class="rateBookAveRage" data-rateyo-rating="{{$slideshow_may_interested_book->book_average_rating}}"></div>

                    </div>
                    <a href="{{ url('/book_page/'.$slideshow_may_interested_book->book_href) }}" > 
                        <button type="button"     class="btn btn-default">Լսել Գիրքը</button>
                    </a>
            </div>  
            @endforeach  
        </div>
</div>
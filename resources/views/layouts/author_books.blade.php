<div class="col-sm-10 book_page_right"  style="margin-top:220px;width:77%;padding-left: 1%;">
    <h4 class="page_title" style="width:280px;"><b>Հեղինակի Բոլոր Գրքերը</b></h4>
      <div class="row center_content">
      @foreach($author_books as $author_book)
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ $author_book->book_image }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>{{ $author_book->book_title }}</b></p>
                <p class="book_author">{{ $author->author_name}}</p>
                <div class="rateBookAveRage" data-rateyo-rating="{{$author_book->book_average_rating}}"></div>

            </div>
                <a href="{{ url('/book_page/'.$author_book->book_href) }}" > <button type="button" class="btn btn-default">Լսել Գիրքը</button></a>      
         </div>
       </div>
      @endforeach 
      </div>  
        
        <!-- <nav aria-label="navigation" style="text-align:center;">
          <ul class="pagination">
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
              </a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          </ul>
        </nav> -->
        
</div>
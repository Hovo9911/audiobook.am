@if (Auth::guest())
    <li class="menu_item login"><a class="cd-signin" href="#">Մուտք</a></li>
@else

<li class="dropdown menu_item">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> {{ Auth::user()->first_name }} <i class="fa fa-angle-down" aria-hidden="true"></i>
    </a>
    <ul class="dropdown-menu">
        <li @if(\Request::is('personal_page')) class="active" @endif><a href="{{ url('/personal_page') }}">Անձնական էջ</a></li>
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();localStorage.removeItem('right_menu');localStorage.setItem('logout','logout');


               document.getElementById('logout-form').submit();">
                Դուրս գալ
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</li>

@endif

<script type="text/javascript">

    if(localStorage.getItem("logout") ){

        // localStorage.removeItem("logout")
        document.location.replace = '/';
        // window.history.forward(1);
    }


</script>

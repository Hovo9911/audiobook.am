<h3>Նախընտրած գրքեր</h3>
       <hr>
       @foreach($lovely_books as $lovely_book)
       <div>
           <div class="image">
               <a href="{{ url('/book_page/'.$lovely_book->book_href) }}" title="{{ $lovely_book->book_title}}">
                   <img src="{{ $lovely_book->book_image }}" class="img-responsive" alt="logo">
               </a>
           </div>
               <span>

               <p class="book_title"><b>
                    <a href="{{ url('/book_page/'.$lovely_book->book_href) }}" title="{{ $lovely_book->book_title}}"> 
                      {{ $lovely_book->book_title }}
                    </a></b></p>
                   @if($lovely_book->author_id)
               <p class="book_author"> Հեղինակ` 
                    <a href="{{ url('/author_page/'.$lovely_book->href) }}" title="{{$lovely_book->author_name}}">
                      {{$lovely_book->author_name }}
                    </a>
                   @endif
              </p>
           </span>
        </div>
        @endforeach
        <a href="{{ url('/favourite_books') }}"> <button type="button" class="btn btn-default btn-xs more_books" style="float:right;">Դիտել Բոլորը</button></a>
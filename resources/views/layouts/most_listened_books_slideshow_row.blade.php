<div class="center" style="max-width:762px;margin:0 auto;">
    @foreach($slideshow_most_listened_books as $slideshow_most_listened_book)
        <div class="books">
            <img src="{{ $slideshow_most_listened_book->book_image }}" class="img-responsive book_img" alt="img">
            <div class="books_info">
                <p class="book_title"><b>{{ \App\Http\Controllers\BookCategoriesController::truncate($slideshow_most_listened_book->book_title, $length=30) }}</b></p>
                <p class="book_author">Հեղինակ` {{ $slideshow_most_listened_book->author_name}}</p>
                <div class="rateBookAveRage" data-rateyo-rating="{{$slideshow_most_listened_book->book_average_rating}}"></div>

            </div>
            <a href="{{ url('/book_page/'.$slideshow_most_listened_book->book_href) }}" >
                <button type="button"     class="btn btn-default">Լսել Գիրքը</button>
            </a>
        </div>
    @endforeach
</div>
<h3>Նոր հեղինակներ</h3>
       <hr>
    @foreach($new_authors as $new_author)
        <div>
            <div class="image">
                <a href="{{ url('/author_page/'.$new_author->href) }}">
                    <img src="{{ $new_author->author_image }}" class="img-responsive" alt="logo">
                </a>
            </div>
       	    <span>
       	        <b>
       	            <a href="{{ url('/author_page/'.$new_author->href) }}"> {{ $new_author->author_name }}</a>
                </b>
            </span>
        </div>
    @endforeach
     <a href="{{ url('/all_authors') }}"> <button  type="button" class="btn btn-default btn-xs more_books" style="float:right;">Դիտել Բոլորը</button></a>
   


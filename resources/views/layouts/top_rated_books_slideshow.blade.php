<div class="filters">
    <div class="row">
        <div class="col-sm-5">
            <h4><b class="under-line">Ամենագնահատված</b></h4>
        </div>
        <div class="col-sm-7">

        </div>
    </div>
    <hr>
    <div id="show_slideshow_by_category">
        @include('layouts.top_rated_books_slideshow_row')
        <a href="{{ url('/top_rated_books') }}">
            <button type="button" class="btn btn-default more_books">Ավելին</button>
        </a>
    </div>
</div>
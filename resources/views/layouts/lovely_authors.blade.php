<h3>Սիրված հեղինակներ</h3>
       <hr>
       @foreach($lovely_authors as $lovely_author)
       <div>
           <div class="image">
               <a href="{{ url('/author_page/'.$lovely_author->href) }}">
                   <img src="{{ $lovely_author->author_image }}" class="img-responsive" alt="logo">
               </a>
           </div>
       	        <span>
       	        	<b>
       	   	            <a href="{{ url('/author_page/'.$lovely_author->href) }}"> {{ $lovely_author->author_name }}</a>
       	   	        </b>
       	   	    </span>
       </div>
       @endforeach
    
       <a href="{{ url('/lovely_authors') }}"> <button  type="button" class="btn btn-default btn-xs more_books" style="float:right;">Դիտել Բոլորը</button></a>
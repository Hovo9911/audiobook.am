<style>
#playlist,audio{background:#a5a4a2;width:400px;padding:20px;}
audio{height: 72px;}  
#playlist{list-style: none;}    
#playlist .active a{color:#000;text-decoration:none;}
#playlist li a{color:#fff;background:rgba(0,0,0,.1);padding:5px;display:block;}
#playlist li a:hover{text-decoration:none;}
video::-internal-media-controls-download-button {
   display:none;
}

video::-webkit-media-controls-enclosure {
    overflow:hidden;
}

video::-webkit-media-controls-panel {
    width: calc(100% + 30px); /* Adjust as needed */
}
</style>

@foreach($books as $value)
    <?php $book = $value->book ?>
    <div class="my_book row">
        <div class="col-sm-3 col-xs-6" style="text-align:center">
            <img src="{{ $book->book_image }}" class="img-responsive my_book_img" alt="img">
        </div>
        <div class="col-sm-9 col-xs-6 my_book_info">
            <div>
                <p class="book_title show-page"><b>{{$book->book_title}}</b></p>
                @if(!is_null($book->author))
                    <p class="book_author  show-page">{{$book->author->author_name}}</p>
                @endif

                <div class="rateBookAveRage" data-rateyo-rating="{{$book->book_average_rating}}"></div>


            </div>
            <br />  <br />
            {{--<p style="margin-bottom: 20px;">Աուդիոգիրքն ամբողջությամբ կարող եք լսել AUDIOBOOK.AM հավելվածի միջոցով:</p>--}}
            {{--<button type="button" class="btn btn-default appstore_btn"><b><i class="fa fa-apple" aria-hidden="true"></i>--}}
                    {{--Բեռնել</b></button>--}}


            @if(!empty($book->records))
            <div class="row">
                <div class="col-md-12">
                    <br />
                    {{--<audio class="audio" controls>--}}
                       {{--@foreach($book->records as $record)--}}
                            {{--<source src="{{url('audio/'.$book->book_href)}}" type="audio/mpeg">--}}
                       {{--@endforeach--}}
                        {{--Your browser does not support the audio element.--}}
                    {{--</audio>--}}
                    

                        {{--<audio id="audio"  preload="auto"  controls="" type="audio/mpeg" controlsList="nodownload">--}}

                            {{--<source type="audio/mp3" src="{{url('audio/'.$book->records[0]->book_record_title)}}">--}}

                        {{--</audio>--}}
                        {{--<ul id="playlist">--}}
                            {{--@foreach($book->records as $key=>$record)--}}
                                {{--<li class=" {{(!$key) ? 'active': ''}}"><a href="{{url('audio/'.$record->book_record_title)}}">{{$record->book_record_title}}</a></li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{----}}
                    
                    
                    
                </div>
            </div>
                @endif
        </div>
    </div>


    @section('scripts')
        <script>
var audio;
var playlist;
var tracks;
var current;

init();
function init(){


    current = 0;
    audio = $('audio');
    playlist = audio.next();
    tracks = playlist.find('li a');
    len = tracks.length - 1;
   

    playlist.find('a').click(function(e){
        e.preventDefault();

       var link = $(this);
        current = link.parent().index();

        run(link, link.closest('ul').prev()[0]);
    });
    audio[0].addEventListener('ended',function(e){
        current++;


        if(current == len){
            current = 0;
            var link = playlist.find('a')[0];
        }else{
            var link = playlist.find('a')[current];
        }
        run($(link),audio[0]);
    });
}
function run(link, player){



    player.src = link.attr('href');
    par = link.parent();

    par.addClass('active').siblings().removeClass('active');
    player.load();
    player.play();
}


        </script>
    @endsection


@endforeach

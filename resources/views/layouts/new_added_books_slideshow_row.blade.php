<div class="center" style="max-width:762px;margin:0 auto;">
    @foreach($slideshow_new_added_books as $slideshow_new_added_book)
        <div class="books">
            <img src="{{ $slideshow_new_added_book->book_image }}" class="img-responsive book_img" alt="img">
            <div class="books_info">
                <p class="book_title"><b>{{ $slideshow_new_added_book->book_title }}</b></p>

                @if($slideshow_new_added_book->author_id)
                    <p class="book_author">Հեղինակ` {{ $slideshow_new_added_book->author_name}}</p>
                @endif

                <div class="rateBookAveRage" data-rateyo-rating="{{$slideshow_new_added_book->book_average_rating}}"></div>

            </div>
            <a href="{{ url('/book_page/'.$slideshow_new_added_book->book_href) }}" >
                <button type="button"     class="btn btn-default">Լսել Գիրքը</button>
            </a>
        </div>
    @endforeach
</div>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Meta's -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=0.25">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Audiobook.am - Գրքերը խոսում են</title>

    @yield('metas')
    <!-- favico -->
    <link rel="icon" type="image/png" href="{{asset('images/favico.png')}}" />

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="{{ asset('public/css/audio.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/please-wait.css') }}"/>
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
<!--    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}"/>
    <link rel="stylesheet" href="{{asset('plugins/stars/jquery.rateyo.min.css')}}" />

    @yield('styles')

    {{--<script src="https://use.fontawesome.com/ad951634f0.js"></script>--}}

    <script>
        window.Laravel = {
            url: '{{url('/')}}'
        }
    </script>

</head>
<body>

@yield('pleaseWait')
@if(\Request::is('book_page/*'))
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/hy_AM/sdk.js#xfbml=1&version=v2.11&appId=196239334284249';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@endif



    <div id="app">
        @if(Session::get('email_error'))
        <div class="alert alert-danger">
            <p>Email allready exist</p>
        </div>
        @endif
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container audio_header">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('images/logo-min1.png') }}" class="img-responsive logo" alt="logo"  width="100px">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                        <div class="dropdown" style="float:left">
                            <button class="btn btn-default dropdown-toggle lang_button" type="button" data-toggle="dropdown"><img src="{{ asset('images/Armenia_flag.png') }}" width="20px"> Հայերեն</button>
                            {{--<span class="caret"></span></button>--}}
                            {{--<ul class="dropdown-menu">--}}
                              {{--<li><a href="#"><img src="{{ asset('images/Russia_flag.png') }}" width="20px"> Ռուսերեն</a></li>--}}
                              {{--<li><a href="#"><img src="{{ asset('images/USA_flag.png') }}" height="19px"> Անգլերեն</a></li>--}}
                            {{--</ul>--}}
                        </div>               
                       {{-- <form class="navbar-form navbar-left" action="{{ url('/categories') }}" style="margin-top:0">
                          <div class="input-group">
                            <input type="text" class="form-control" placeholder="Որոնում" width="100px">
                            <div class="input-group-btn">
                              <button class="btn btn-default" type="button">
                                <i class="glyphicon glyphicon-search"></i>
                              </button>
                            </div>
                          </div>
                        </form>--}}
                        <ul class="nav navbar-nav navbar-right audio_menu_opened" style="position:absolute;right:20px;display:none;">
                            <li><a href="{{ url('/categories') }}">Գրքեր</a></li>
                            <li><a href="#">Հեղինակներ</a></li>
                            <li><a href="#">Անձնական էջ</a></li>
                            <li><a href="#">Մեր Մասին</a></li>
                            <li><a href="#">Կապ</a></li>
                            <span class="glyphicon glyphicon-remove close" ></span>
                        </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right audio_menu_closed header-right-menu">
                        <!-- Authentication Links -->

                              <li @if(\Request::is('books')) class="active" @endif ><a href="{{ url('/books') }}">Գրքեր</a></li>
                              <li @if(\Request::is('all_authors')) class="active" @endif><a href="{{ url('/all_authors') }}">Հեղինակներ</a></li>
                              <li @if(\Request::is('about')) class="active" @endif><a href="{{ url('/about') }}">Մեր Մասին</a></li>
<!--                              <li><a href="{{ url('/faq') }}">Հ․Տ․Հ</a></li>-->
                              <li @if(\Request::is('contact')) class="active" @endif><a href="{{ url('/contact') }}">Կապ</a></li>
<!--
                              <li><a href="{{ route('login') }}">Մուտք</a></li>
                              <li><a href="{{ route('register') }}">Գրանցում</a></li>
-->
                              @include('layouts/header-menu-user-name')

                    </ul>
                </div>
            </div>
        </nav>
            @include('errors.alerts')
	<div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-user-modal-container container-fluid"> <!-- this is the container wrapper -->
           <div class="row"> 
             <div class="col-xs-12 form_content">  
                <ul class="cd-switcher">
                    <li><a href="#0">Մուտք Գործեք Ձեր Անձնական Էջ</a></li>
                    <li><a href="#0">Ստեղծել Անձնական Էջ</a></li>
                </ul>

                <div id="cd-login" class="register_content form"> <!-- log in form -->

                    <div class="alert alert-danger hide" id="login-error-alerts"> </div>
                    <div class="alert alert-success hide" id="login-success-alerts"> </div>

                    <form method="POST" action="{{ route('login') }}" id="login-form">
                      {{ csrf_field() }}
                      <input type="hidden" name="book" value="" class="book-auth">
                      <div class="field-wrap{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label>
                          Մուտքանուն<span class="req">*</span>
                        </label>
                          <input id="email" type="email" name="email" value="{{ old('email') }}" oninput="InvalidMsg(this);" oninvalid="InvalidMsg(this);" required>
                      </div>

                      <div class="field-wrap{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label>
                          Գաղտնաբառ<span class="req">*</span>
                        </label>
                        <input id="password" type="password" autocomplete="new-password" name="password" oninvalid="InvalidMsg(this);" required>
                      </div>
                        
                      <div class="row">  
                        <div class="col-sm-6">
                            <label class="modal_checkbox"> Հիշել
                                  <input type="checkbox" name="remember" />
                                  <div class="modal_checkbox_indicator"></div>
                            </label>
                        </div> 
                        <div class="col-sm-6">
                            <p class="cd-form-bottom-message"><a href="#0">Մոռացե՞լ եք գաղտնաբառը:</a></p>
                        </div>  
                      </div> 
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif 
                         @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif   
                        
                      <button type="submit" class="button button-block">Մուտք</button>

                      </form>
                      <a href="{{url('auth/facebook')}}" class="face_login button"> Login with Facebook</a>
                </div> <!-- cd-login -->

                <div id="cd-signup" class="register_content form"> <!-- sign up form -->
                    
                <form  method="post"  action="{{ route('register') }}" id="register-form">
                   {{ csrf_field() }}

                     <div class="alert alert-success hide" id="register-success-alerts"></div>
                      <div class="col-sm-6">

                          <div class="field-wrap{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label>
                              Անուն<span class="req">*</span>
                            </label>
                            <input id="first_name" type="text" name="first_name" value="{{ old('first_name') }}" oninvalid="InvalidMsg(this);" required>
                              <span class="help-block first_name-error"></span>
                          </div>


                          <div class="field-wrap{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>
                              Էլ.փոստ<span class="req">*</span>
                            </label>
                            <input id="email_reg" type="email" name="email_reg" value="{{ old('email_reg') }}" oninput="InvalidMsg(this);" oninvalid="InvalidMsg(this);" required>
                            <span class="help-block email-error"></span>
                          </div>
                          
                          <div class="form-group" style="padding:0;margin-bottom: 40px;height:38px">
                            <label class="col-sm-5 control-label" style="margin-left:0;font-size:17px;">Ընտրել երկիրը</label>
                            <div class="col-sm-7">
                            <select id="country_id" name="country" class="form-control" style="font-size:17px;"><option value="0"></option><option value="7">Ալբանիա</option><option value="235">ԱՄՆ</option><option value="20">Ավստրալիա</option><option value="21">Ավստրիա</option><option value="17">Արգենտինա</option><option value="27">Բելառուս</option><option value="28">Բելգիա</option><option value="34">Բոսնիա և Հերցեգովինա</option><option value="41">Բուլղարիա</option><option value="37">Բրազիլիա</option><option value="87">Գերմանիա</option><option value="91">Գրենլանդիա</option><option value="65">Դանիա</option><option value="70">Եգիպտոս</option><option value="74">Էստոնիա</option><option value="227">Թուրքիա</option><option value="228">Թուրքմենստան</option><option value="108">Ինդոնեզիա</option><option value="111">Իռլանդիա</option><option value="106">Իսլանդիա</option><option value="208">Իսպանիա</option><option value="112">Իսրայել</option><option value="114">Իտալիա</option><option value="109">Իրան</option><option value="110">Իրաք</option><option value="126">Լատվիա</option><option value="181">Լեհաստան</option><option value="250">Լեռնային Ղարաբաղի Հանրապետություն</option><option value="132">Լիտվա</option><option value="61">Խորվաթիա</option><option value="46">Կանադա</option><option value="63">Կիպրոս</option><option value="124">Կիրգիզիա</option><option value="62">Կուբա</option><option value="18">Հայաստան</option><option value="107">Հնդկաստան</option><option value="102">Հոլանդիա</option><option value="90">Հունաստան</option><option value="105">Հունգարիա</option><option value="119">Ղազախստան</option><option value="116">Ճապոնիա</option><option value="233">ՄԱԷ (Միացյալ Արաբական Էմիիրատներ)</option><option value="135">Մակեդոնիա</option><option value="148">Մեքսիկա</option><option value="234">Միացյալ Թագավորություն</option><option value="150">Մոլդովա</option><option value="152">Մոնղոլիա</option><option value="161">Նիդեռլանդներ</option><option value="170">Նորվեգիա</option><option value="214">Շվեդիա</option><option value="215">Շվեյցարիա</option><option value="238">Ուզբեկստան</option><option value="232">Ուկրաինա</option><option value="64">Չեխիա</option><option value="153">Չերնոգորիա</option><option value="52">Չինաստան</option><option value="172">Պակիստան</option><option value="182">Պորտուգալիա</option><option value="186">Ռումինիա</option><option value="187">Ռուսաստան</option><option value="16">Սաուդյան Արաբիա</option><option value="216">Սիրիա</option><option value="203">Սլովակիա</option><option value="210">Սուդան</option><option value="240">Վատիկան</option><option value="241">Վենեսուելա</option><option value="242">Վիետնամ</option><option value="86">Վրաստան</option><option value="218">Տաջիկստան</option><option value="179">Ֆիլիպիններ</option><option value="80">Ֆինլանդիա</option><option value="81">Ֆրանսիա</option></select>
                            </div>  
                           </div>
      
                          <div class="field-wrap{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label>
                              Գաղտնաբառ<span class="req">*</span>
                            </label>
                            <input id="password_reg" type="password" name="password_reg" oninvalid="InvalidMsg(this);" required>
                              <span class="help-block password-error"></span>

                          </div>

                          </div>   
                       <div class="col-sm-6">  
                              
                          <div class="field-wrap{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label>
                              Ազգանուն<span class="req">*</span>
                            </label>
                            <input id="last_name" type="text" name="last_name" value="{{ old('last_name') }}" oninvalid="InvalidMsg(this);" required>
                              <span class="help-block last_name-error"></span>

                          </div>
                          <div class="field-wrap">
                            <label>
                              Հեռախոսահամար
                            </label>
                            <input type="text" name="telephone" autocomplete="off"/>
                          </div>

                          <div class="form-group" style="height:38px;margin-bottom:40px;text-align: left;">
                             <label class="col-sm-5 control-label" style="margin-left:0;font-size:17px;">Ընտրել սեռը</label>
                               <div class="col-sm-7">
                              <label class="radio-inline"><input type="radio" name="gender" value="male" checked>Արական</label>
                              <label class="radio-inline"><input type="radio" name="gender" value="female">Իգական</label>
                              </div>
                          </div>     
                          <div class="field-wrap">
                            <label>
                              Հաստատել Գաղտնաբառը<span class="req">*</span>
                            </label>
                            <input id="password-confirm" type="password" name="password_confirmation" oninvalid="InvalidMsg(this);" required>
                          </div>
                          </div>
                           @if ($errors->has('email') && $errors->first('email')!="These credentials do not match our records.")
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif  
                          <div class="row">	
                                <button type="submit" class="button button-block" style="display:inline-block;max-width:240px;margin-right:15px;">Գրանցվել</button>
                          </div>	


                          </form>   
                    
                </div> <!-- cd-signup -->

                <div id="cd-reset-password" class="register_content"> <!-- reset password form -->

                    <div id="reset-alerts"></div>
                    <p class="cd-form-message">Մոռացե՞լ եք գաղտնաբառը: Խնդրում ենք մուտքագրել Ձեր էլ.փոստի հասցեն և մենք Ձեզ կուղարկենք նամակ՝ գաղտնաբառը վերականգնելու հրահանգներով: </p>

                    <form class="cd-form" id="reset-form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        
                        
                          <div class="group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email_forget" type="email" name="email" value="{{ old('email') }}" oninput="InvalidMsg(this);" oninvalid="InvalidMsg(this);" required><span class="login_input highlight"></span><span class="bar"></span>
                            <label>Էլ.փոստ</label>
                          </div>

                        <button type="submit" class="button button-block">ՎԵՐԱԿԱՆԳՆԵԼ ԳԱՂՏՆԱԲԱՌԸ</button>
                    </form>

                    <p class="cd-form-bottom-message"><a href="#0">Վերադառնալ նախորդ էջ</a></p>
                </div> <!-- cd-reset-password -->
                <a href="#0" class="cd-close-form">Close</a>
             </div>    
           </div>    
		</div> <!-- cd-user-modal-container -->
	</div> <!-- cd-user-modal -->
        @yield('content')
     <section class="page_footer">  
        <footer class="container">
        <div class="row" style="padding: 0 100px;">
            <div class="col-sm-3 contact_block">
            <h4>Մեր Կոնտակտները</h4>
            <p>Հասցե՝ Ամիրյան 3/20</p>
            <p>Հեռ.՝ +374 60 512 538</p> 
             <div class="social_links "> 
                <a href="https://www.facebook.com/Audiobook.am/" class="icon-button facebook" target="_blank"><i class="fa fa-facebook icon-facebook"></i><span></span></a>
                <a href="https://www.instagram.com" class="icon-button instagram" target="_blank"><i class="fa fa-instagram icon-instagram"></i><span></span></a>
                <a href="https://twitter.com" class="icon-button twitter" target="_blank"><i class="fa fa-twitter icon-twitter"></i><span></span></a>
            </div>    
            </div> 
            <div class="col-sm-3">
            <h4>Օգնություն</h4>
<!--            <a href="{{ url('/faq') }}">Հաճախ Տրվող Հարցեր</a> -->
            <a href="{{ url('/terms_of_use') }}">Ընդհանուր Դրույթներ</a>     
            <a href="{{ url('/privacy_policy') }}">Տվյալների Գաղտնիություն</a>   
            </div>  
            <div class="col-sm-3">
            <h4>Մեր Մասին</h4>
            <a href="{{ url('/about') }}">Նախագծի Մասին</a>    
            <a href="{{ url('/contact') }}">Հետադարձ Կապ</a>    
            </div>
            <div class="col-sm-3">
            <h4>Հղումներ</h4>
            <a href="{{ url('/books') }}">Գրքեր</a>    
            <a href="{{ url('/all_authors') }}">Հեղինակներ</a>                       
            </div>             
        </div>
        </footer>
     </section>   
    <section style="height:60px;background: #e6e6e6;padding-top:22px;color:#999999;">   
        <div class="container">
            <span > © <?php echo date('Y');?> AUDIOBOOK.AM Բոլոր իրավունքները պաշտպանված են</span>
            <span style="float:right;">Կայքի մշակումը` <a href="http://smartsoft.am/" target="_blank">SmartSoft LLC</a></span>
        </div>
    </section>   
    </div>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{asset('js/jquery-ui.js')}}"></script>
    {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script  src="{{ asset('js/login_modal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
    <script src="{{ asset('js/login.js') }}"></script>
<script src="{{asset('plugins/sweetalert2/sw.js')}}"></script>
    <script src="{{ asset('js/ajax-functions.js') }}"></script>
    <script src="{{ asset('plugins/stars/jquery.rateyo.min.js') }}"></script>
    <script src="{{ asset('js/audio.js') }}"></script>
    @yield('scripts')




</body>
</html>

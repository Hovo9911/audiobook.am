@extends('layouts.app')

@section('content')
    <section class="page_content">
        <div class="container">

            <div class="row">
                <div class="col-sm-2 left_content" style="width:23%">
                    <div class="left_content_1" style="margin-top: 28px;">
                        @include('layouts.lovely_authors')
                    </div>
                    <div class="left_content_2">
                        @include('layouts.lovely_books')
                    </div>
                </div>
                <div class="col-sm-10" style="width:77%;padding-left:1%;">

                    <div class="row">
                        <form id="author_search_form" method="GET" action="/books">
                        <div class="col-sm-8">
                            <ul class="books_nav nav nav-pills">
                                <li class="active">
                                    <input {{(empty($_GET['book_category']) || ($_GET['book_category'] == 'all')) ? 'checked':'' }} type="radio"  name="book_category" id="all" value="all">

                                    <label for="all">
                                        Բոլորը
                                    </label>
                                </li>

                                @foreach($books_categories as $book_categories)
                                    @if($book_categories -> id != 9)
                                        <li>
                                            <input {{(!empty($_GET['book_category']) && ($_GET['book_category'] ==$book_categories -> name))? 'checked':''}} id="{{$book_categories -> name}}" type="radio" name="book_category" value="{{$book_categories -> name}}">

                                            <label for="{{$book_categories -> name}}">
                                                {{$book_categories -> name}}
                                            </label>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group suggest" >
                                    <input style="max-width: 197px;" type="text" value="{{trim((!empty($_GET['book_name']) ? $_GET['book_name'] : ''))}}" class="form-control authorsAndBooksAutocomplete" name="book_name"
                                           id="book_name">
                                    <span style="display:none"></span>
                                    <span style="display:none"></span>
                                    <span style="display:none"></span>
                                    {{--<input type="hidden" name="authororbook" id="authororbook" value="">--}}


                                    <span class="input-group-btn" style="border-radius: 25px;display:inherit">
                                        <button class="btn btn-secondary" type="sumbit"><i class="glyphicon glyphicon-search"
                                                                       style="margin-right: 5px;"></i></button>
                                    </span>
                            </div>
                        </div>

                    </form>
                    </div>
                    <div class="row center_content tab-content">
                        <div class="tab-pane fade in active" id="9">
                            @include('layouts.books')
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </section>
@section('scripts')
    <script src="{{ asset('js/authorandbook.js') }}"></script>
    
    <script>
        $(function () {
            $('input[name="book_category"]').change(function () {
                $(this).closest('form').submit();
            })
        })
    </script>
@endsection
@endsection





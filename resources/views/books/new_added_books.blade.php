@extends('layouts.app')

@section('content')
<section class="page_content">
<div class="container">

    <div class="row">
    <div class="col-sm-2 left_content" style="width:23%">
    <div class="left_content_1">
      @include('layouts.lovely_authors') 
    </div>
    <div class="left_content_2">
      @include('layouts.lovely_books')
    </div>
    </div>
    <div class="col-sm-10" style="width:77%;padding-left:1%;">
    <h4 class="page_title" style="width:27%"><b>Նոր Ավելացված Գրքեր</b></h4>  
      <div class="row center_content">
          @foreach($new_added_books as $new_added_book)
            <div class="col-sm-4">
              <div class="books">
                   <img src="{{ $new_added_book->book_image }}" class="img-responsive book_img" alt="img">
                      <div class="books_info">
                          <p class="book_title"><b>{{ $new_added_book->book_title }}</b></p>

                          @if($new_added_book->author_id)
                          <p class="book_author">Հեղինակ` {{ $new_added_book->author_name}}</p>
                          @endif
                          <div class="rateBookAveRage" data-rateyo-rating="{{$new_added_book->book_average_rating}}"></div>

                      </div>
                      <a href="{{ url('/book_page/'.$new_added_book->book_href) }}" > 
                          <button type="button"     class="btn btn-default">Լսել Գիրքը</button>
                      </a>
              </div> 
            </div>   
          @endforeach 
       </div>
        <nav aria-label="navigation" style="text-align:center;">
          <ul class="pagination">
            <li class="page-item">
                {{ $new_added_books->links() }}
              </a>
            </li>
          </ul>
        </nav>
    
    </div>
    </div>
</div>
</section>
@endsection

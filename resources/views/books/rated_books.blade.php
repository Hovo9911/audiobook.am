@extends('layouts.app')

@section('content')
<section class="page_content">
<div class="container">
    
    <div class="row">
    <div class="col-sm-2 left_content" style="width:23%">
    <div class="left_content_1" style="margin-top:0;">
       <h3>Սիրված հեղինակներ</h3>
       <hr>
       <div><img src="{{ asset('images/Yeghishe_Charents_Armenian_poet.jpg') }}" class="img-responsive" alt="logo"> <span><b><a href="{{ url('/author_page') }}"> Եղիշե Չարենց</a></b></span></div>
       <div><img src="{{ asset('images/jacklondon.jpg') }}" class="img-responsive" alt="logo"> <span><b><a href="{{ url('/author_page') }}"> Ջեկ Լոնդոն</a></b></span></div>
       <div><img src="{{ asset('images/Yeghishe_Charents_Armenian_poet.jpg') }}" class="img-responsive" alt="logo"> <span><b><a href="{{ url('/author_page') }}"> Եղիշե Չարենց</a></b></span></div>
       <a href="{{ url('/lovely_authors') }}"> <button  type="button" class="btn btn-default btn-xs more_books" style="float:right;">Դիտել Բոլորը</button></a>
    </div>
    </div>
    <div class="col-sm-10" style="width:77%;padding-left:1%;">
      <div class="filters" style="margin-top: 5px;">
      <div class="row">
        <div class="col-sm-5">
        <h4 style="padding-top: 4px;"><b>Նախընտրած գրքեր</b></h4>    
        </div>    
        <div class="col-sm-7">   
          <ul class="nav nav-pills">
            <li class="active"><a data-toggle="pill" href="#rated_books_1">Բոլոր Ժանրեր</a></li>
            <li><a data-toggle="pill" href="#rated_books_2">Հեքիաթներ</a></li>
            <li><a data-toggle="pill" href="#rated_books_3">Արձակ</a></li>
            <li><a data-toggle="pill" href="#rated_books_4">Պոեզիա</a></li>
          </ul>
         </div>     
      </div>
 

          <hr>
          <div class="tab-content audio_pill">
            <div id="rated_books_1" class="tab-pane fade in active">
      <div class="center_content">
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
            <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a> 
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a> 
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
            <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
       </div>
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
            <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>  
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div> 
        </div>
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>   
        </div>
       </div>  
            </div>
            <div id="rated_books_2" class="tab-pane fade">
      <div class="center_content">
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
       </div>
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div> 
        </div>
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
            <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>   
        </div>
       </div>  
            </div>
            <div id="rated_books_3" class="tab-pane fade">
      <div class="center_content">
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
            <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a> 
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a> 
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
       </div>
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div> 
        </div>
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>   
        </div>
       </div>  
            </div>
            <div id="rated_books_4" class="tab-pane fade">
      <div class="center_content">
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
       </div>
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">>
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a> 
         </div> 
        </div>
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a> 
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
            </div>
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>   
        </div>
       </div>  
            </div>
          </div>
      </div>
        
        <nav aria-label="navigation" style="text-align:center;">
          <ul class="pagination">
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
              </a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          </ul>
        </nav>
        
      </div>
    </div>
</div>
    
</section>   

@endsection

@extends('layouts.app')

@section('content')
    <?php
    use App\Http\Controllers\BookCategoriesController;
    ?>

    @section('styles')
        <link rel="stylesheet" href="//cdn.plyr.io/2.0.13/plyr.css">
        <link rel="stylesheet" href="{{ asset('css/audioplayer.css') }}">

    @endsection



    <section class="book_page_banner">
        <div class="container">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-8">


                    @foreach($books as $book)



                        @section('metas')

                            <meta property="og:image" content="{{asset($book->book_image)}}" />
                            <meta property="og:title" content="{{ $book->book_title}}" />
                            <meta property="og:description" content="{{ $book->book_description}}" />
                        @endsection

                    <?php


                        $times = [];
                        foreach (BookCategoriesController::show_book_records($book->book_id) as $key => $book_records) {
                            ++$key;
                            $tracks[] = '{
                         "track": ' . $key . ',
                         "name": "' . $book_records->book_record_title . '",
                         "length": "00:50",
                         "file": "' . $book_records->book_record . '"
                      }';

                            $times[] = $book_records->book_record_duration;

                        }
                        $tracks_string = implode(',', $tracks);


                        $time_sum = BookCategoriesController::AddPlayTime($times);

                        $time_sum_exp = explode(':',$time_sum);




                        ?>
                        <div class="row banner_content" style="position:absolute;top:120px;">
                            <div class="col-sm-5 col-xs-6" style="text-align:center">
                                <img src="{{ $book->book_image }}" class="book_page_img" alt="img">
                                <div class="book_social_links">

                                    <a data-mobile-iframe="true" href="https://www.facebook.com/sharer/sharer.php?u=http://audiobook.am/book_page/{{$book->book_href}}/?v={{uniqid()}}&display=popup" OnClick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=400'); return false;" class="fb-share"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="https://twitter.com/share?url=http://audiobook.am/book_page/{{$book->book_href}}&amp;text={{substr( $book->book_description, 0, 140 )}} " title="{{$book->book_title}}" OnClick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=400'); return false;" class="tw-share"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://audiobook.am/book_page/{{$book->book_href}}&description={{$book->book_description}}"  title="{{$book->book_title}}" OnClick="window.open(this.href,'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=400'); return false;" class="in-share"><i class="fa fa-linkedin" aria-hidden="true"></i></a>

                                    {{--<div class="fb-share-button" data-href="http://audiobook.am/book_page/{{$book->book_href}}/?v={{uniqid()}}" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Faudiobook.am%2Fbook_page%2FMtnadzor&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Поделиться</a></div>--}}
                                </div>

                                <button type="button" class="btn btn-default appstore_btn"><b><i class="fa fa-apple"
                                                                                                 aria-hidden="true"></i>
                                        Բեռնել</b></button>
                            </div>
                            
                            <div class="col-sm-7 col-xs-6 banner_text" style="padding-right:0;">
                                <div style="height:200px;">
                                    <p class="book_title show-page"  style="font-size:23px"><b>{{ $book->book_title}}</b></p>

                                   @if($book->author_id)
                                    <p class="book_author show-page" style="font-size:15px;">Հեղինակ ` {{ $book->author_name}}</p>
                                   @endif

                                    <p>Ընդհ. տևողություն՝ {{($time_sum_exp[0] != 00) ? (int)$time_sum_exp[0].' Ր' : ''}} {{($time_sum_exp[1] != 00) ? (int)$time_sum_exp[1].' ՎՐԿ' : ''}} </p>
                                    <div class="stars {{(!Auth::check()) ? 'open-login-modal' : ''}}">
                                        <div id="rateBook" data-rateyo-rating="{{$average_rate}}"
                                             @if($user_rated) data-rateyo-read-only="true" @endif ></div>
                                        <div id="rateBookAveRage" data-rateyo-read-only="true"></div>
                                    </div>
                                    <form action="/confirmation" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="book_id" id="book_id" value="{{ $book -> book_id}}">
                                        {{--<button  type="submit" class="btn btn-default buy_btn {{(!Auth::check()) ? 'open-login-modal' : ''}}"><b>Գնել {{ $book->book_price}} ԴՐ</b></button>--}}


                                       @if(!$purchased_book)
                                        <a href="{{url('confirmation/'.$book -> book_id)}}"
                                           class="btn btn-default buy_btn {{(!Auth::check()) ? 'open-login-modal' : ''}}"><b>Գնել {{ $book->book_price}}
                                                ԴՐ</b></a>
                                        @else
                                            <span class="already-purchased">Արդեն գնված է</span>
                                        @endif
                                    </form>
                                    <?php

                                    $check_book = (in_array($book->book_id, $user_wish_list_books) ? true : false)
                                    ?>

                                    <a {{($check_book) ?  "href=".url('personal_page#favorite_authors').""  : '' }}  class="btn btn-default wishlist_btn {{($check_book) ? ' all-ready-wish-list' : 'add-wish-list' }} {{(!Auth::check()) ? 'open-login-modal' : ''}}"><b><i
                                                    class="fa  {{($check_book) ? ' fa-check ' : 'fa-bookmark' }}"
                                                    aria-hidden="true"></i> {{($check_book) ? ' Նախընտրած' : 'Հիշել' }}
                                        </b></a>
                                    {{--<a href="{{url('confirmation/'.$book -> book_id)}}" {{(!Auth::check()) ? 'data-id='.$book -> book_id : ''}} class="btn btn-default wishlist_btn {{(!Auth::check()) ? 'open-login-modal' : ''}}"><b><i class="fa fa-bookmark" aria-hidden="true"></i> Հիշել</b></a>--}}

                                </div>
                                <p class="audio_book_text"
                                   style="text-align: justify;height: 170px;">{{ $book->book_description}}</p>

                                <div class="container audioplayer">
                                    <div class="column add-bottom">
                                        <div id="mainwrap">
                                            <div id="nowPlay">
                                                <span class="left" id="npAction">Լսել գրքից մի հատված</span>
                                                <span class="right" id="npTitle"></span>
                                            </div>
                                            <div id="audiowrap">
                                                <div id="audio0">
                                                    <audio preload id="audio1" controls="controls">Your browser does not
                                                        support HTML5 Audio!
                                                    </audio>
                                                </div>
                                                <div id="tracks" style="display:none;">
                                                    <a id="btnPrev" class="aplayer_a">&larr;</a>
                                                    <a id="btnNext" class="aplayer_a">&rarr;</a>
                                                </div>
                                            </div>
                                            <div id="plwrap">
                                                <ul id="plList" style="padding:0 20px;"></ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!--            <div id="audio_player"></div> -->
                            </div>
                        </div>


                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section class="page_content">
        <div class="container">
            <div class="row">
                <div class="col-sm-2 left_content" style="width:24%">
                    <div class="left_content_1">
                        @include('layouts.lovely_authors')
                    </div>
                    <div class="left_content_2">
                        @include('layouts.lovely_books')
                    </div>
                </div>
                <div class="col-sm-10 book_page_right book_page_carusel"
                     style="margin-top: 342px;width:76%;padding-left: 50px;">
                    @include('layouts/may_interested_books_slideshow')
                    <div class="row">
                        <h4><b style="border-bottom:3px solid #ff6600;padding-bottom:5px;margin-top:50px;">Կարծիքներ</b>
                        </h4>
                        <hr>

                        <div class="fb-comments" data-href="http://audiobook.am/personal_page" data-width="100%"
                             data-numposts="5"></div>

                    </div>
                </div>
            </div>
        </div>
    </section>

@section('scripts')
    <script src="//api.html5media.info/1.1.8/html5media.min.js"></script>
    <script src="//cdn.plyr.io/2.0.13/plyr.js"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            'use strict'
            var supportsAudio = !!document.createElement('audio').canPlayType;
            if (supportsAudio) {
                var index = 0,
                    playing = false,
                    mediaPath = 'http://audiobook.am/uploads/tmp/',
                    extension = '',
                    tracks = [<?=$tracks_string?>],
//            , {
//                "track": 2,
//                "name": "The Forsaken - Broadwing Studio (Final Mix)",
//                "length": "8:31",
//                "file": "BS_TF"
//            }

                    buildPlaylist = $.each(tracks, function (key, value) {
                        var trackNumber = value.track,
                            trackName = value.name,
                            trackLength = value.length;
                        if (trackNumber.toString().length === 1) {
                            trackNumber = '0' + trackNumber;
                        } else {
                            trackNumber = '' + trackNumber;
                        }
                        $('#plList').append('<li><div class="plItem"><div class="plNum">' + trackNumber + '.</div><div class="plTitle">' + trackName + '</div><div class="plLength">' + trackLength + '</div></div></li>');
                    }),
                    trackCount = tracks.length,
                    npAction = $('#npAction'),
                    npTitle = $('#npTitle'),
                    audio = $('#audio1').bind('play', function () {
                        playing = true;
                        npAction.text('Լսել գրքից մի հատված');
                    }).bind('pause', function () {
                        playing = false;
                        npAction.text('Լսել գրքից մի հատված');
                    }).bind('ended', function () {
                        npAction.text('Լսել գրքից մի հատված');
                        if ((index + 1) < trackCount) {
                            index++;
                            loadTrack(index);
                            audio.play();
                        } else {
                            audio.pause();
                            index = 0;
                            loadTrack(index);
                        }
                    }).get(0),
                    btnPrev = $('#btnPrev').click(function () {
                        if ((index - 1) > -1) {
                            index--;
                            loadTrack(index);
                            if (playing) {
                                audio.play();
                            }
                        } else {
                            audio.pause();
                            index = 0;
                            loadTrack(index);
                        }
                    }),
                    btnNext = $('#btnNext').click(function () {
                        if ((index + 1) < trackCount) {
                            index++;
                            loadTrack(index);
                            if (playing) {
                                audio.play();
                            }
                        } else {
                            audio.pause();
                            index = 0;
                            loadTrack(index);
                        }
                    }),
                    li = $('#plList li').click(function () {
                        var id = parseInt($(this).index());
                        if (id !== index) {
                            playTrack(id);
                        }
                    }),
                    loadTrack = function (id) {
                        $('.plSel').removeClass('plSel');
                        $('#plList li:eq(' + id + ')').addClass('plSel');
                        npTitle.text(tracks[id].name);
                        index = id;
                        audio.src = mediaPath + tracks[id].file + extension;
                    },
                    playTrack = function (id) {
                        loadTrack(id);
                        audio.play();
                    };
                extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
                loadTrack(index);
            }


            var Stars = $("#rateBook");
            var StarAverage = $('#rateBookAveRage');

            Stars.rateYo({
                halfStar: true,
                starWidth: "20px",
                normalFill: "#ffffff",
                ratedFill: "#f54a07",

            });

            Stars.rateYo()
                .on("rateyo.set", function (e, data) {
                    var rating = data.rating;
                    $.ajax({
                        url: Laravel.url + '/book/rate',
                        method: 'POST',
                        data: {book_id: $('#book_id').val(), rating: rating},
                        dataType: 'JSON',
                        success: function (response) {
                            if (response.status == 'success') {

                                Stars.rateYo("destroy");

                                StarAverage.rateYo({
                                    rating: response.rate,
                                    starWidth: "20px",
                                    normalFill: "#ffffff",
                                    ratedFill: "#f54a07",
                                });

                                StarAverage.next(response.rate)
                            }

                        }
                    })

                });
        });

        //initialize plyr
        plyr.setup($('#audio1'), {});
    </script>
@endsection

@endsection
@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/checkout.css') }}"/>
    <section class="page_content">
        <div class="container">

            <div class="row">
                <div class="col-sm-2 left_content" style="width:23%">
                    <div class="left_content_1" style="margin-top: 28px;">
                        @include('layouts.lovely_authors')
                    </div>
                    <div class="left_content_2">
                        @include('layouts.lovely_books')
                    </div>
                </div>
                <div class="col-sm-10" style="width:77%;padding-left:1%;">
                    @foreach($books_info as $book_info)
                        <div class="row confirm_block">


                            <table id="cart" class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th style="width:50%">Գիրք</th>
                                    <th style="width:50%">Գին</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td data-th="Product" style="padding: 30px;">
                                        <div class="row">
                                            <div class="col-sm-5 hidden-xs"><img src="{{ $book_info->book_image}}"
                                                                                 alt="..." class="img-responsive"/>
                                            </div>
                                            <div class="col-sm-7" style="padding-top:50px;">
                                                <p class="book_title"><b><a href="/book_page/{{$book_info->book_href}}"
                                                                            title="{{$book_info->book_title}}">{{$book_info->book_title}}  </a></b>
                                                </p>
                                                @if($book_info->author_id)
                                                <p class="book_author show-page"> Հեղինակ ` <a
                                                            href="/author_page/{{$book_info->href}}"
                                                            title="{{$book_info->author_name}}">{{$book_info->author_name}}</a>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                    <td data-th="Price">{{ $book_info->book_price}} ԴՐ</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>

                        <div class="row payment_block">
                            <h3>Վճարման տարբերակ</h3>
                            <hr>
                            <div class="block_content">
                                <p style="margin-bottom:30px;">Խնդրում ենք ընտրել վճարման տարբերակը.</p>
                                <ul class="nav nav-pills">
                                    <li class="active credit_card">
                                        <a class="credit_card_a" data-toggle="pill" href="#payment">
                                            <img src="{{asset('images/credit-card.png')}}" alt="icon"
                                                 class="default_icon"> Credit Card
                                        </a>
                                    </li>

                                    <li class="cash">
                                        <a class="paypal_a" data-toggle="pill" href="#payment3">
                                            <img src="{{asset('images/Paypal.png')}}" alt="icon" class="default_icon">
                                        </a>
                                    </li>
                                </ul>


                                <p style="margin-top: 40px;">Վճարումը հաստատելուց հետո գիրքը կհայտնվի Ձեր Անձնական էջի
                                    ԻՄ ԳՐՔԵՐԸ բաժնում:
                                    Գիրքն ամբողջությամբ Դուք կարող եք լսել AUDIOBOOK.AM հավելվածի միջոցով:
                                    Պարզապես անհրաժեշտ է App Store հարթակից ներբեռնել AUDIOBOOK.AM հավելվածը Ձեր
                                    iPhone-ի կամ iPad-ի համար և մուտք գործել հավելվածի Անձնական էջ, որտեղ ԻՄ
                                    ԳՐՔԵՐԸ բաժնում Դուք կգտնեք աուդիոգրքի ամբողջական տարբերակը: Մաղթում ենք
                                    հաճելի ունկնդրում:</p>


                            </div>
                        </div>

                        <div class="row btn_block">
                            <form action="/checkout" method="POST" style="padding: 0 40px;">
                                {{csrf_field()}}

                                <label class="checkbox">Կարդացել եմ և ընդունում եմ AUDIOBOOK.AM կայքից և հավելվածից
                                    օգտվելու պայմանները:
                                    <input style="left: 36px;top: 0px;" type="checkbox" class="read-accept"
                                           name="read-accept" required>
                                    <div class="checkbox_indicator"></div>
                                </label>

                                <input type="hidden" name="pay_type" id="pay_type" value="credit_card">
                                <input type="hidden" name="book_id" value="{{$book_info->book_id}}">
                                <div style="text-align:center;margin-top:30px;">
                                    <button type="submit" class="btn main_btn"><span class="pay">Վճարել </span>
                                        <span class="priceAMD">{{ $book_info->book_price}} ԴՐ</span>
                                        <span class="priceEURO hide">{{ number_format($book_info->book_price / $currency->rate,2)}} <i class="fa  fa-euro"></i></span>
                                    </button>
                                </div>
                            </form>
                        </div>


                    @endforeach
                </div>


            </div>
        </div>
    </section>
@section('scripts')
    <script type="text/javascript">



        var loading = false;
        var ReadAcceptCheckbox = $(".read-accept");

        if(ReadAcceptCheckbox.is(':checked')){
            loading = true;
        }

        ReadAcceptCheckbox.change(function () {
            loading = false;
            if (this.checked) {
                loading = true;
            }
        });



        var MainBtn = $('.main_btn');
        MainBtn.removeClass('disabled');
        MainBtn.click(function () {
            if (loading) {
                $(this).addClass('disabled')
                $(this).prepend('<i class="fa fa-spinner fa-spin"></i> ')
            }
        });

        var PriceAMD = $('.priceAMD');
        var PriceEURO = $('.priceEURO');

        $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            // alert(target);
            PriceAMD.addClass('hide')
            PriceEURO.addClass('hide')
            if (target == '#payment') {
                $('#pay_type').val('credit_card');
                PriceAMD.removeClass('hide')

            } else if (target == '#payment3') {
                $('#pay_type').val('paypal');
                PriceEURO.removeClass('hide')
            }

        });
    </script>
@endsection
@endsection

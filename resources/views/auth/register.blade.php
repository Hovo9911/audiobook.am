@extends('layouts.app')

@section('content')
<div class="container-fluid register_content">
  <div class="form" style="text-align:right; max-width: 900px;">

          <h4 class="under_title" style="line-height:25px;max-width: 240px;margin-bottom:40px;text-align: left;margin-left:15px;"><b>Ստեղծել Անձնական Էջ</b></h4> 
          
          <form action="/" method="post" class="form-horizontal">
          
          <div class="col-sm-6">

          <div class="field-wrap">
            <label>
              Անուն<span class="req">*</span>
            </label>
            <input type="text" required autocomplete="off"/>
          </div>
          <div class="field-wrap">
            <label>
              Էլ.փոստ<span class="req">*</span>
            </label>
            <input type="email" required autocomplete="off"/>
          </div>
          <div class="form-group" style="padding:0;margin-bottom: 40px;height:38px">
              <label class="col-sm-5 control-label" style="margin-left:0;font-size:17px;">Ընտրել երկիրը</label>
              <div class="col-sm-7">
            <select id="country_id" name="country_id" class="form-control" style="font-size:17px;"><option value="0"></option><option value="7">Ալբանիա</option><option value="235">ԱՄՆ</option><option value="20">Ավստրալիա</option><option value="21">Ավստրիա</option><option value="17">Արգենտինա</option><option value="27">Բելառուս</option><option value="28">Բելգիա</option><option value="34">Բոսնիա և Հերցեգովինա</option><option value="41">Բուլղարիա</option><option value="37">Բրազիլիա</option><option value="87">Գերմանիա</option><option value="91">Գրենլանդիա</option><option value="65">Դանիա</option><option value="70">Եգիպտոս</option><option value="74">Էստոնիա</option><option value="227">Թուրքիա</option><option value="228">Թուրքմենստան</option><option value="108">Ինդոնեզիա</option><option value="111">Իռլանդիա</option><option value="106">Իսլանդիա</option><option value="208">Իսպանիա</option><option value="112">Իսրայել</option><option value="114">Իտալիա</option><option value="109">Իրան</option><option value="110">Իրաք</option><option value="126">Լատվիա</option><option value="181">Լեհաստան</option><option value="250">Լեռնային Ղարաբաղի Հանրապետություն</option><option value="132">Լիտվա</option><option value="61">Խորվաթիա</option><option value="46">Կանադա</option><option value="63">Կիպրոս</option><option value="124">Կիրգիզիա</option><option value="62">Կուբա</option><option value="18">Հայաստան</option><option value="107">Հնդկաստան</option><option value="102">Հոլանդիա</option><option value="90">Հունաստան</option><option value="105">Հունգարիա</option><option value="119">Ղազախստան</option><option value="116">Ճապոնիա</option><option value="233">ՄԱԷ (Միացյալ Արաբական Էմիիրատներ)</option><option value="135">Մակեդոնիա</option><option value="148">Մեքսիկա</option><option value="234">Միացյալ Թագավորություն</option><option value="150">Մոլդովա</option><option value="152">Մոնղոլիա</option><option value="161">Նիդեռլանդներ</option><option value="170">Նորվեգիա</option><option value="214">Շվեդիա</option><option value="215">Շվեյցարիա</option><option value="238">Ուզբեկստան</option><option value="232">Ուկրաինա</option><option value="64">Չեխիա</option><option value="153">Չերնոգորիա</option><option value="52">Չինաստան</option><option value="172">Պակիստան</option><option value="182">Պորտուգալիա</option><option value="186">Ռումինիա</option><option value="187">Ռուսաստան</option><option value="16">Սաուդյան Արաբիա</option><option value="216">Սիրիա</option><option value="203">Սլովակիա</option><option value="210">Սուդան</option><option value="240">Վատիկան</option><option value="241">Վենեսուելա</option><option value="242">Վիետնամ</option><option value="86">Վրաստան</option><option value="218">Տաջիկստան</option><option value="179">Ֆիլիպիններ</option><option value="80">Ֆինլանդիա</option><option value="81">Ֆրանսիա</option></select>
            </div>  
           </div>
          <div class="field-wrap">
            <label>
              Գաղտնաբառ<span class="req">*</span>
            </label>
            <input type="password" required autocomplete="off"/>
          </div>
              
          </div>   
          <div class="col-sm-6">
          <div class="field-wrap">
            <label>
              Ազգանուն<span class="req">*</span>
            </label>
            <input type="text" required autocomplete="off"/>
          </div>
          <div class="field-wrap">
            <label>
              Հեռախոսահամար<span class="req">*</span>
            </label>
            <input type="text" autocomplete="off"/>
          </div>
<!--
            <div class="form-group" style="padding:0;margin-bottom: 40px;height:38px">
              <label class="col-sm-5 control-label" style="margin-left:0;font-size:17px;">Ընտրել երկիր:</label>
              <div class="col-sm-7">
-->
          <div class="form-group" style="height:38px;margin-bottom:40px;text-align: left;">
             <label class="col-sm-5 control-label" style="margin-left:0;font-size:17px;">Ընտրել սեռը</label>
               <div class="col-sm-7">
              <label class="radio-inline"><input type="radio" name="optradio">Արական</label>
              <label class="radio-inline"><input type="radio" name="optradio">Իգական</label>
              </div>
          </div>     
          <div class="field-wrap">
            <label>
              Հաստատել Գաղտնաբառը<span class="req">*</span>
            </label>
            <input type="password" required autocomplete="off"/>
          </div>
          </div>
          <div class="row social_register_btn">	
					<div class="col-sm-8">
							<button type="button" class="btn btn-primary" style="background:#3b5998"><i class="fa fa-facebook"></i></button>

							<button type="button" class="btn btn-danger"  style="background: linear-gradient(to right, rgb(251, 173, 80) 0%, rgb(188, 42, 141) 100%);"><i class="fa fa-instagram"></i></button>

							<button type="button" class="btn btn-info" style="background:#0077B5"><i class="fa fa-linkedin"></i></button> 

					</div>
            <div class="col-sm-4">
              <button type="submit" class="button button-block" style="display:inline-block;max-width:240px;margin-right:15px;">Գրանցվել</button>
					</div>
          </div>	

          
          </form>
      
</div> <!-- /form -->
</div>
@endsection
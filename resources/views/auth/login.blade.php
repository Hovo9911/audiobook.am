@extends('layouts.app')

@section('content')
<div class="container-fluid register_content">
  <div class="form" style="text-align: center;">

          <h4 class="under_title" style="line-height:25px;max-width: 320px;margin-bottom:40px;text-align:left;"><b>Մուտք Գործեք Ձեր Անձնական Էջ</b></h4> 
          
          <form  method="POST" action="{{ route('login') }}" >
          
          {{ csrf_field() }}
          <div class="field-wrap">
            <label>
              Մուտքանուն<span class="req">*</span>
            </label>
            <input type="email" name="email" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Գաղտնաբառ<span class="req">*</span>
            </label>
            <input type="password" name="password" required autocomplete="off"/>
          </div>
          <div class="row" style="margin-bottom:20px;">  
            <div class="col-sm-6">
                <div class="checkbox"  style="text-align:left;">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Հիշել
                    </label>
                </div>
            </div> 
            <div class="col-sm-6">
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Մոռացե՞լ եք գաղտնաբառը:
                    </a>
            </div>  
          </div> 
          <button type="submit" class="button button-block">Մուտք</button>
          
          </form>

                    
</div> <!-- /form -->
</div>
@endsection
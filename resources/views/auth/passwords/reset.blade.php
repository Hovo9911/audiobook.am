@extends('layouts.app')

@section('content')
<section class="page_content" style="min-height: 500px;">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="box-shadow: 0 0 20px rgba(19, 35, 47, 0.15);">
                <h4 class="under_title" style="line-height:25px;max-width: 230px;margin-left: 265px;"><b>Փոխել Գաղտնաբառը</b></h4>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="reset-email" class="col-md-4 control-label">Էլ.փոստ</label>

                            <div class="col-md-6">
                                <input id="reset-email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>               
                                
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="reset-password" class="col-md-4 control-label">Նոր Գաղտնաբառ</label>

                            <div class="col-md-6">
                                <input id="reset-password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="reset-password-confirm" class="col-md-4 control-label">Հաստատել Գաղտնաբառը</label>
                            <div class="col-md-6">
                                <input id="reset-password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary change-pass-btn">
                                    Փոխել Գաղտնաբառը
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection

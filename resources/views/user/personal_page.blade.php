@extends('layouts.app')

@section('content')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/sweetalert2/sw.css') }}"/>

    <style>
      /*  audio::-webkit-media-controls-enclosure {
            overflow:hidden;
        }

        audio::-webkit-media-controls-panel {
            width: calc(100% + 10px); !* Adjust as needed *!
        }*/
    </style>
@endsection
<section class="page_content">
    <div class="container faq_content">
        <div class="row">
            <div class="col-md-3"
                 style="background:#fff;padding:30px 20px;box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);">
                <div style="border-bottom:1px solid #FFA500;padding-bottom:20px;padding-left:25px">
                    <img src="{{ asset('images/user.png') }}" width="50px"><span
                            style="color:#999999;font-size: 18px;margin-left: 10px;"> {{ Auth::user()->first_name }}</span>
                </div>
                <br>
                <ul class="nav nav-pills nav-stacked faq_nav">
                    <li class="active"><a href="#tab_d" data-toggle="pill">Իմ Գրքերը</a></li>
                    <li><a href="#favorite_authors" data-toggle="pill">Նախընտրած Գրքեր</a></li>
                    <li><a href="#tab_a" data-toggle="pill">Անձնական տվյալներ </a></li>
                    <li><a href="#tab_b" data-toggle="pill">Վճարման տվյալներ</a></li>
                    @if(Auth::user()->id == 5 or Auth::user()->id == 97)
                        <li><a href="#purchases" data-toggle="pill">Պատվերներ</a></li>
                    @endif
                </ul>
            </div>
            <div class="tab-content col-md-9">
                <div class="tab-pane faq_tab" id="tab_a">
                    <p class="under_title" style="font-size:16px;max-width: 200px"><b>Անձնական տվյալներ</b></p><br>

                    <div class="personal-info-error alert alert-danger hide"></div>
                    <form style="max-width:350px;" method="post" action="{{url('user/personal_info')}}"
                          id="personal-info">
                        <input type="hidden" name="_method" value="put">
                        {{csrf_field()}}
                        <div>
                            <div class="form-group">
                                <label for="first_namee">Անուն</label>
                                <input type="text" class="form-control" name="first_name" id="first_namee"
                                       value="{{Auth::user()->first_name}}">
                            </div>
                            <div class="form-group">
                                <label for="last_namee">Ազգանուն</label>
                                <input type="text" class="form-control" name="last_name" id="last_namee"
                                       value="{{Auth::user()->last_name}}">
                            </div>
                            <div class="form-group">
                                <label for="email1">Էլ․փոստ</label>

                                <?php
                                    $email = Auth::user()->email;
                                    if(!empty(Auth::user()->facebook_user_id)){
                                        $f_email = @explode('_',Auth::user()->email);
                                        $email = @$f_email[0];
                                    }
                                ?>

                                <input type="email" class="form-control" id="email1" disabled
                                       value="{{@$email}}">
                            </div>
                            <div class="form-group">
                                <label for="phone">Հեռախոսահամար</label>
                                <input type="text" class="form-control" id="phone" name="telephone"
                                       value="{{Auth::user()->telephone}}">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-default personal_page_buttons">Խմբագրել</button>

                    </form>


                </div>
                <div class="tab-pane faq_tab" id="tab_b">
                    <p class="under_title" style="font-size:16px;max-width: 200px"><b>Վճարման տվյալներ</b></p><br>
                    

                        <div class="row" id="payment_details">

                            @if(count($purchased_books) > 0)

                                    <table class="table table-hover table-condensed">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Անվանում</th>
                                            <th>Հեղինակ</th>
                                            <th>Գնման օր</th>
                                            <th>Գին</th>
                                            <th>Վճարման Տարբերակ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($purchased_books as $value)
                                            <?php $book = $value->book; ?>
                                        <tr>
                                            <td data-th="Product" style="padding: 30px;">
                                                <img src="{{$book->book_image}}" alt="..." class="img-responsive book_image"/>
                                            </td>
                                            <td><a href="#tab_d" class="purchased-book" data-toggle="pill"> {{$book->book_title}}</a></td>


                                            <td>@if(!is_null($book->author)) {{ $book->author->author_name}}@endif</td>
                                            <td>{{date('F j, Y',strtotime($book->updated_at))}}</td>
                                            <td data-th="Price">{{$book->book_price}} ԴՐ</td>
                                            <td>
                                                @if($value->payment_type == 'credit_card')
                                                <img src="{{asset('images/credit-card.png')}}" alt="icon">Credit Card
                                                @elseif($value->payment_type == 'paypal')

                                                    <img width="100px" src="{{asset('images/Paypal.png')}}" alt="icon">
                                                @endif



                                            </td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                @else

                                <div class="no-item">
                                    <h3>Դուք չունեք կատարված վճարումներ:  </h3>
                                </div>


                            @endif

                        </div>

                </div>

                <div class="tab-pane active faq_tab" id="tab_d">
                    <p class="under_title" style="font-size:16px;max-width: 150px"><b>Գնված Գրքեր</b></p><br>

                    @if(count($purchased_books) > 0)
                        @include('layouts.my_book',['books'=>$purchased_books])
                    @else
                        <div class="no-item">
                            <h3>Դուք չունեք գնված աուդիոգրքեր:  </h3>
                            <p>AUDIOBOOK.AM-ը հնարավորություն է ստեղծում, չկտրվելով առօրեական զբաղմունքներից, զուգահեռաբար ծանոթանալու նախընտրած գրական տեղծագործությանը: Մեր կայքում ներկայացված են հայ և համաշխարհային գրականության լավագույն ստեղծագործությունների հնչունավորումները:</p>
                            <a href="{{ url('/books') }}" class="btn btn-default orange-btn">ԴԻՏԵԼ ԲՈԼՈՐ ԳՐՔԵՐԸ</a>
                        </div>
                    @endif




                </div>
                <div class="tab-pane faq_tab" id="favorite_authors">
                    <p class="under_title" style="font-size:16px;max-width: 210px"><b>Նախնտրած Գրքեր</b></p><br>
                 

                 <div class="row">

                    @if(count($favorite_books))
                        @foreach($favorite_books as $value)
                            <?php $book = $value->book ?>

                            <div class="col-sm-6 book-content">
                                <div class="books favorite-books">
                                    <i class="fa fa-times-circle fa-2x remove-book-wish-list" data-id="{{$value->book->book_id}}" aria-hidden="true"></i>

                                    <img src="{{ $book->book_image }}" class="img-responsive book_img" alt="logo">
                                    <div class="books_info">
                                        <p class="book_title show-page"><b>{{ $book->book_title }}</b></p>
                                        @if(!is_null($book->author))
                                        <p class="book_author">Հեղինակ ` {{ $book->author->author_name}}</p>
                                        @endif
                                        <div class="rateBookAveRage" data-rateyo-rating="{{$book->book_average_rating}}"></div>

                                    </div>
                                    <a href="{{ url('/book_page/'.$book->book_href) }}">
                                        <button type="button" class="btn btn-default">Իմանալ Ավելին</button>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @else

                         <div class="no-item">
                             <h3>Դուք չունեք նախընտրած աուդիոգրքեր: </h3>
                             <p>AUDIOBOOK.AM կայքը պարբերաբար թարմացվում է, և ԳՐՔԵՐ բաժնում Դուք կարող եք գտնել հայ և համաշխարհային գրականության լավագույն ստեղծագործությունների հնչունավորումները:</p>
                             <a href="{{ url('/books') }}" class="btn btn-default orange-btn">ԴԻՏԵԼ ԲՈԼՈՐ ԳՐՔԵՐԸ</a>
                         </div>
                     @endif
                </div>
                </div>
                @if(Auth::user()->id == 5 or Auth::user()->id == 97)
                <div class="tab-pane faq_tab" id="purchases">
                    <p class="under_title" style="font-size:16px;max-width: 200px"><b>Վաճառք</b></p><br>


                    <div class="row" id="payment_details">
                        @if(count($all_trying_to_purchase_books) > 0)

                            <table class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th>Պատվերի #</th>
                                    <th>Հաճախորդ</th>
                                    <th>Կարգավիճակ</th>
                                    <th>Գին</th>
                                    <th>Վճարման Տարբերակ</th>
                                    <th>Վճարման ամսաթիվ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($all_trying_to_purchase_books as $value)
                                    <?php
                                        $book  = $value->book;
                                        $buyer = $value->buyer;
//                                        var_dump(gettype($buyer));

                                    ?>
                                    <tr>
                                        <td>{{$value->order_id}}</td>
                                        <td>{{$buyer->first_name." ".$buyer->last_name}}</td>
                                        <td>
                                            @if($value->payment_status == 'success')
                                                <span class="label label-success">{{$value->payment_status}}</span>
                                            @else
                                                <span class="label label-warning">{{$value->payment_status}}</span>
                                            @endif

                                        </td>
                                        <td data-th="Price">{{$book->book_price}} ԴՐ</td>
                                        <td>
                                            @if($value->payment_type == 'credit_card')
                                                <img src="{{asset('images/credit-card.png')}}" alt="icon">Credit Card
                                            @elseif($value->payment_type == 'paypal')
                                                <img width="100px" src="{{asset('images/Paypal.png')}}" alt="icon">
                                            @endif
                                        </td>
                                        <td>{{date('F j, Y',strtotime($value->created_at))}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @endif
                    </div>

                </div>
                @endif
            </div><!-- tab content -->
        </div>
    </div>
</section>


@endsection

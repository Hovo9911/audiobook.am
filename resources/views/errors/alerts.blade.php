@if(session()->has('success'))
    <div class="alert alert-success auto_close">
        <div class="success-message">
            {{session('success')}}
        </div>
    </div>
@endif

@if(session()->has('success-reg'))
    <div class="alert alert-success">
        <div class="success-message">
            {{session('success-reg')}}
        </div>
    </div>
@endif


@if(session()->has('error'))

    <div class="alert alert-danger">
        <h4 style="margin-bottom: 0">{{session('error')}}</h4>
    </div>
@endif

@if (session('warning'))
    <div class="alert alert-warning">
        {{ session('warning') }}
    </div>
@endif


@extends('layouts.app')

@section('content')

    @section('pleaseWait')
<!--preloader-->
<script src="{{ asset('js/please-wait.min.js') }}"></script>
<script type="text/javascript">
  window.loading_screen = window.pleaseWait({
    logo: "images/logo-min1.png",
    loadingHtml: "<div class='sk-circle'><div class='sk-circle1 sk-child'></div><div class='sk-circle2 sk-child'></div><div class='sk-circle3 sk-child'></div><div class='sk-circle4 sk-child'></div><div class='sk-circle5 sk-child'></div><div class='sk-circle6 sk-child'></div><div class='sk-circle7 sk-child'></div><div class='sk-circle8 sk-child'></div><div class='sk-circle9 sk-child'></div><div class='sk-circle10 sk-child'></div><div class='sk-circle11 sk-child'></div><div class='sk-circle12 sk-child'></div></div>"
  });

</script>
@endsection
<!--preloader-->
<section class="page_content">
 <div class="main" style="margin:0 auto;max-width:1349px">
     {{--{{dd($slideshow_books)}}--}}

 @foreach($slideshow_books as $index => $slideshow_book)
    <div class="item active row audio_slide" style="background: url(<?=$slideshow_book->slideshow_background_image?>);">
         <div class="col-xs-6" style="position:relative;">
            <img src="{{ $slideshow_book->book_image }}" class="slide_img" alt="img">
         </div>
         <div class="col-xs-6">
          <p class="book_title show-page"><b>{{ $slideshow_book->book_title }}</b></p>
            <p class="book_author show-page">Հեղինակ` {{ $slideshow_book->author_name }}</p>
             <div class="rateBookAveRage" data-rateyo-rating="{{$slideshow_book->book_average_rating}}"></div>

             <p class="audio_book_text">{{\App\Http\Controllers\BookCategoriesController::truncate($slideshow_book->book_description,250)}}</p>
            <?php
               $button_color='color:'.$slideshow_book->slideshow_button_color;
            ?>
            <a href="{{ url('/book_page/'.$slideshow_book->book_href) }}" > <button type="button" style="<?=$button_color?>" class="btn btn-default audio_slide_<?=$index?>">Լսել Գիրքը  </button></a>
         </div>
     </div>
     @endforeach
    <!-- <div class="item active row audio_slide" style="background: linear-gradient(to right,  #0082c8 0%,#667db6 100%);">
         <div class="col-xs-6" style="position:relative;">
            <img src="{{ asset('images/night_swimmer.png') }}" class="slide_img" alt="img">
         </div>
         <div class="col-xs-6">
          <p class="book_title"><b>Գիշերային լողորդը</b></p>
            <p class="book_author">Հեղինակ։ Հանսյորգ Շերթենլայբ</p>
            <img src="{{ asset('images/star5_white.png') }}" alt="img"  width="70px" class="stars">
            <p class="audio_book_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default blue_button">Իմանալ Ավելին</button></a>
         </div>
     </div> 
    <div class="item active row audio_slide">
         <div class="col-xs-6" style="position:relative;">
            <img src="{{ asset('images/white_fang.png') }}" class="slide_img" alt="img">
         </div>
         <div class="col-xs-6">
          <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
            <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
            <img src="{{ asset('images/star5_white.png') }}" alt="img"  width="70px" class="stars">
            <p class="audio_book_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
         </div>
     </div>  -->
</div>  
    
<div class="container">
    
    <div class="row">
    <div class="col-sm-2 left_content" style="width:24%">
        
        
    <div class="left_content_1" style="margin-top: 55px;">
       @include('layouts.new_authors')  
    </div>
    
    <div class="left_content_2" style="margin-top: 111px;">
       @include('layouts.lovely_books')
    </div>
    </div>
    <div class="col-sm-10 right_content main_carusels" style="width:76%;">     
     @include('layouts.new_added_books_slideshow')
     @include('layouts.most_listened_books_slideshow')
     @include('layouts.top_rated_books_slideshow')  
    </div>
    </div>
</div>
</section>
@section('scripts')
<script>
$(document).ready(function(){
$('.main').slick({
  centerMode: true,
  centerPadding: '380px',
  slidesToShow: 1,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '80px',
        slidesToShow: 1
      }
    }
  ]
});
});
window.loading_screen.finish();

</script>

@endsection
@endsection



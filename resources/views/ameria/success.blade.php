@extends('layouts.app')

@section('content')
<section class="page_content_paypal">
    <div class="container success"> 
       <div class="icon_block"> 
        <i class="fa fa-check " aria-hidden="true"></i>
       </div>
        <h3>Ձեր վճարումը հաջողությամբ կատարվել է</h3>
        <p>Աուդիոգիրքն արդեն հասանելի է Ձեր Անձնական էջի ԻՄ ԳՐՔԵՐ բաժնում:<br> Մաղթում ենք հաճելի ունկնդրում:</p>
        <a href="{{ url('/personal_page') }}" class="btn btn-default orange-btn">ԴԻՏԵԼ ԻՄ ԳՐՔԵՐԸ</a>
    </div>
</section>
@endsection    
@extends('layouts.app')

@section('content')
<section class="page_content_paypal">
    <div class="container error"> 
       <div class="icon_block"> 
        <i class="fa fa-exclamation" aria-hidden="true"></i>
       </div>
        <h3>Ձեր վճարումը չի հաջողվել</h3>
        <p>Հնարավոր է, որ Ձեր վճարումը չի հաջողվել մի քանի պատճառով.</p> 
        <div style="width:400px;margin:0 auto;text-align:left;">
        <p>1) Վճարային քարտի սխալ տվյալներ<br> 
2) Քարտին բավականաչափ գումարի բացակայություն</p> 
        </div>
        <p>Եթե համոզված եք, որ վերոնշյալ կետերը ճիշտ են, ապա խնդրում ենք փորձել մի փոքր ուշ:<br> Հարցերի դեպքում՝ գրեք մեզ կամ զանգահարեք:</p>
    </div>
</section>
@endsection    
@extends('layouts.app')

@section('content')

    <div class="page_content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                <form action="{{url('admin/book')}}" method="post" enctype="multipart/form-data">

                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="author_id">Author</label>
                        <select required class="form-control" name="author_id" id="author_id">
                            <option></option>
                            @foreach($authors as $author)
                                <option value="{{$author->auth_id}}">{{$author->author_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="author_id">Book Category</label>
                        <select required class="form-control" name="book_category" id="book_category">
                            <option></option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="book_price">Book Price</label>
                        <input required type="number" value="2990.00" name="book_price" class="form-control" id="book_price">
                    </div>
                    <div class="form-group">
                        <label for="book_title">Book Title:</label>
                        <input required type="text" name="book_title" class="form-control" id="book_title">
                    </div>


                    <div class="form-group">
                        <label for="book_description">Book Description:</label>
                        <textarea required class="form-control" rows="5" id="book_description" name="book_description"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="book_description">Image:</label>
                        <input type="file" name="book_image" required>
                    </div>

                    <div class="form-group">
                        <label for="book_href">Book href:</label>
                        <input type="text" name="book_href"  class="form-control" id="book_href">
                    </div>

                    <div class="form-group">
                        <label for="book_language">Book Language:</label>
                        <input type="text" name="book_language" value="am" class="form-control" id="book_language">
                    </div>

                    <div class="form-group">
                        <label for="slideshow">Slide show:</label>
                        <input type="text" name="slideshow" class="form-control" value="0" id="slideshow">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="book_record_title">Book Record title:</label>
                        <input type="text" name="book_record_title" class="form-control" id="book_record_title">
                    </div>

                    <div class="form-group">
                        <label for="book_record_duration">Book Record Duration:</label>
                        <input type="text"  value="00:00" name="book_record_duration" class="form-control" id="book_record_duration">
                    </div>

                    <div class="form-group">
                        <label for="book_record">Book Record Path:</label>
                        <input type="text" name="book_record" class="form-control"  id="book_record">
                    </div>


                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>






@endsection

@section('scripts')
    <script>
        $('#book_title').blur(function () {
            $('#book_record_title').val($(this).val())
        })
    </script>


@endsection
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body style="text-align: center;">

<div style="width: 600px;border: 2px solid #FFA500;margin: 0 auto;padding: 50px;">
<img src="http://audiobook.am/images/logo_email.png" style="width:100px!important;">
<div style="width: 500px;background: #fff;margin: 0 auto;padding: 50px;line-height: 20px;color: #585656;text-align: left;">

    <p>Հարգելի <?php echo $data['first_name'].' '.$data['last_name']?>,</p>
    <p>Ձեր վճարումը հաջողությամբ կատարվել է:</p>

    <p> «<?php echo $data['book_name']?>» աուդիոգիրքն արդեն հասանելի է Ձեր Անձնական էջի ԻՄ ԳՐՔԵՐ բաժնում:</p>
    <p>Աուդիոգիրքն ամբողջական տարբերակով կարող եք լսել AUDIOBOOK-ի հավելվածի միջոցով:</p>

    <p>Մաղթում ենք հաճելի ունկնդրում: </p>
    <p>Հարգանքով,<br> AUDIOBOOK </p>
</div>    
</div>

</body>
</html>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body style="text-align: center;">

<div style="width: 600px;border: 2px solid #FFA500;margin: 0 auto;padding: 50px;">
<img src="http://audiobook.am/images/logo_email.png" style="width:200px;">
<div style="width: 500px;background: #fff;margin: 0 auto;padding: 50px;line-height: 20px;color: #585656;text-align: left;">

    <p>Հարգելի <?php echo $data['first_name'].' '.$data['last_name']?>,</p>
    <p>AUDIOBOOK-ի կայքում Դուք ստեղծել եք Անձնական էջ: Այստեղ Դուք կարող եք պահել Ձեր նախնտրած գրքերը, դիտել գնված գրքերն ու Ձեր վճարման տվյալները: ԻՄ ԳՐՔԵՐ էջի բոլոր աուդիոգրքերն ամբողջական տարբերակով Դուք կարող եք լսել AUDIOBOOK-ի հավելվածի միջոցով: </p>
    <p>Մաղթում ենք հաճելի ունկնդրում: </p>
    <p>Հարգանքով,<br> AUDIOBOOK </p>
</div>    
</div>

</body>
</html>
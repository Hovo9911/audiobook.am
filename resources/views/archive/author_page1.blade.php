@extends('layouts.app')

@section('content')

<section class="book_page_banner">
<div class="container">
<div class="row">
  <div class="col-sm-3"></div>  
  <div class="col-sm-8">
        <div class="row banner_content" style="position:absolute;top:120px;">
        <div class="col-sm-5" style="text-align:center;margin-top:70px;">
            <img src="{{ $authors->author_image }}" alt="img" style="width:260px;box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.2);">
        </div>
        <div class="col-sm-7 banner_text" style="margin-top:70px;">
        <p class="book_title" style="font-size:25px"><b>{{ $authors->author_name}}</b></p>
            
            <div class="stars">
                  <form action="">
                    <input class="star star-5" id="star-5" type="radio" name="star"/>
                    <label class="star star-5" for="star-5"></label>
                    <input class="star star-4" id="star-4" type="radio" name="star"/>
                    <label class="star star-4" for="star-4"></label>
                    <input class="star star-3" id="star-3" type="radio" name="star" checked/>
                    <label class="star star-3" for="star-3"></label>
                    <input class="star star-2" id="star-2" type="radio" name="star" />
                    <label class="star star-2" for="star-2"></label>
                    <input class="star star-1" id="star-1" type="radio" name="star"/>
                    <label class="star star-1" for="star-1"></label>
                  </form>
            </div>
            
            <p class="audio_book_text" style="margin-top:50px;">{{ $authors->author_biography}}</p>
        </div>
        </div>  
   </div>  
</div>
</div>
</section>

<section class="page_content">
    
<div class="container">
    <div class="row">
    <div class="col-sm-2 left_content" style="width:23%">
    <div class="left_content_1">
       <h3>Սիրված հեղինակներ</h3>
       <hr>
       <div><img src="{{ asset('images/Yeghishe_Charents_Armenian_poet.jpg') }}" class="img-responsive" alt="logo"> <span><b><a href="{{ url('/author_page') }}"> Եղիշե Չարենց</a></b></span></div>
       <div><img src="{{ asset('images/Yeghishe_Charents_Armenian_poet.jpg') }}" class="img-responsive" alt="logo"> <span><b><a href="{{ url('/author_page') }}"> Եղիշե Չարենց</a></b></span></div>
       <div><img src="{{ asset('images/Yeghishe_Charents_Armenian_poet.jpg') }}" class="img-responsive" alt="logo"> <span><b><a href="{{ url('/author_page') }}"> Եղիշե Չարենց</a></b></span></div>
        <a href="{{ url('/lovely_authors') }}"> <button  type="button" class="btn btn-default btn-xs more_books" style="float:right;">Դիտել Բոլորը</button></a>
    </div>
    <div class="left_content_2">
       <h3>Նախընտրած գրքեր</h3>
       <hr>
       <div>
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive" alt="logo">
           <span>
               <p class="book_title"><b><a href="{{ url('/book_page') }}"> Սպիտակ ժանիքը</a></b></p>
               <p class="book_author"> Հեղինակ։<a href="{{ url('/author_page') }}">Ջեկ Լոնդոն</a></p>
           </span>
        </div>
       <div><img src="{{ asset('images/white_fang.png') }}" class="img-responsive" alt="logo"> 
            <span>
               <p class="book_title"><b><a href="{{ url('/book_page') }}"> Սպիտակ ժանիքը</a></b></p>
               <p class="book_author"> Հեղինակ։<a href="{{ url('/author_page') }}">Ջեկ Լոնդոն</a></p>
           </span>
        </div>
       <div><img src="{{ asset('images/white_fang.png') }}" class="img-responsive" alt="logo">
            <span>
               <p class="book_title"><b><a href="{{ url('/book_page') }}"> Սպիտակ ժանիքը</a></b></p>
               <p class="book_author"> Հեղինակ։<a href="{{ url('/author_page') }}">Ջեկ Լոնդոն</a></p>
           </span>
        </div>
        <a href="{{ url('/categories') }}"> <button type="button" class="btn btn-default btn-xs more_books" style="float:right;">Դիտել Բոլորը</button></a>
    </div>
    </div>
    <div class="col-sm-10 book_page_right"  style="margin-top:220px;width:77%;padding-left: 1%;">
    <h4 class="page_title" style="width:280px;"><b>Հեղինակի Բոլոր Գրքերը</b></h4>
      <div class="row center_content">
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>
       </div>
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div> 
        </div>
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>
          <div class="books">
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                 <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="logo"  width="70px">
                <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
            </div>
         </div>   
        </div>
       </div>  
        
        <nav aria-label="navigation" style="text-align:center;">
          <ul class="pagination">
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
              </a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          </ul>
        </nav>
        
    </div>
    </div>
</div>
    
</section>
@endsection
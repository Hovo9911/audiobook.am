@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/player_style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/player_demo.css') }}"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('js/jquery.jplayer.js') }}"></script>
    <script src="{{ asset('js/ttw-music-player-min.js') }}"></script>
    <script src="{{ asset('js/myplaylist.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var description = 'Պարույր Սևակի բանաստեղծությունների անդրանիկ գրքույկում («Անմահները հրամայում են», 1948) երևան են եկել բանաստեղծական խառնվածքի և մտածողության բնորոշ գծերը՝ քաղաքացիական անհանգստություն, մարդու ներաշխարհի գաղտնիքները ճանաչելու և բանաստեղծական անկաշկանդ կառուցվածքների դիմելու ձգտում։';

            $('#audio_player2').ttwMusicPlayer(myPlaylistAnmahner, {
                autoPlay:false, 
                description:description,
                jPlayer:{
                    swfPath:'../js/jquery-jplayer' //You need to override the default swf path any time the directory structure changes
                }
            });
        });
    </script>


<section class="book_page_banner" style="background:linear-gradient(to right, rgb(204, 204, 204) 0%, rgb(140, 140, 140) 100%)">
<div class="container">
<div class="row">
  <div class="col-sm-3"></div>  
  <div class="col-sm-8">
<!--
        <div class="row banner_content" style="position:absolute;top:120px;">
        <div class="col-sm-4 col-xs-6" style="text-align:center">
            <img src="{{ asset('images/white_fang.png') }}" class="book_page_img" alt="img" style="width:250px;box-shadow: rgba(19, 35, 47, 0.2) 0px 4px 10px 4px;">
            <button type="button" class="btn btn-default" style="border-radius:15px;margin-top:10px;width:100%;max-width:250px; padding: 8px 12px;background-color:#ff6600;color:#fff"><b>Գնել Գիրքը</b></button>
            <button type="button" class="btn btn-default appstore_btn"><b><i class="fa fa-apple" aria-hidden="true"></i> Բեռնել</b></button>
            <div class="social_links" style="background:#fff;margin: 10px auto 0;border-radius:15px;padding: 5px 0;max-width:250px;">
            <a href="#"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>   
            <a href="#"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>    
            <a href="#"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a>    
            <a href="#"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>    
            </div> 
        </div>
        <div class="col-sm-8 col-xs-6 banner_text" style="margin-top:70px;">
        <p class="book_title" style="font-size:20px"><b>Սպիտակ ժանիքը</b></p>
            <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
            
            <div class="stars">
                  <form action="">
                    <input class="star star-5" id="star-5" type="radio" name="star"/>
                    <label class="star star-5" for="star-5"></label>
                    <input class="star star-4" id="star-4" type="radio" name="star"/>
                    <label class="star star-4" for="star-4"></label>
                    <input class="star star-3" id="star-3" type="radio" name="star" checked/>
                    <label class="star star-3" for="star-3"></label>
                    <input class="star star-2" id="star-2" type="radio" name="star" />
                    <label class="star star-2" for="star-2"></label>
                    <input class="star star-1" id="star-1" type="radio" name="star"/>
                    <label class="star star-1" for="star-1"></label>
                  </form>
            </div>
            
            <p class="audio_book_text" style="margin-top:50px;text-align: justify;padding-right: 40px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
           
            
        </div>
        </div>  
-->

          <div id="audio_player2" style="margin-top: 200px;position: absolute;"></div> 
            <button type="button" class="btn btn-default buy_btn"><b>Գնել Գիրքը 10,0 &#8364;</b></button>
            <button type="button" class="btn btn-default appstore_btn"><b><i class="fa fa-apple" aria-hidden="true"></i> Բեռնել</b></button>
            <div class="book_social_links">
            <a href="#"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>   
            <a href="#"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>    
            <a href="#"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a>    
            <a href="#"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>    
            </div> 
   </div>  
</div>
</div>
</section>

<section class="page_content">
<div class="container">
    <div class="row">
    <div class="col-sm-2 left_content" style="width:23%">
    <div class="left_content_1">
       <h3>Սիրված հեղինակներ</h3>
       <hr>
       <div><img src="{{ asset('images/Yeghishe_Charents_Armenian_poet.jpg') }}" class="img-responsive" alt="logo"> <span><b><a href="{{ url('/author_page') }}"> Եղիշե Չարենց</a></b></span></div>
       <div><img src="{{ asset('images/Yeghishe_Charents_Armenian_poet.jpg') }}" class="img-responsive" alt="logo"> <span><b><a href="{{ url('/author_page') }}"> Եղիշե Չարենց</a></b></span></div>
       <div><img src="{{ asset('images/Yeghishe_Charents_Armenian_poet.jpg') }}" class="img-responsive" alt="logo"> <span><b><a href="{{ url('/author_page') }}"> Եղիշե Չարենց</a></b></span></div>
       <a href="{{ url('/all_authors') }}"> <button  type="button" class="btn btn-default btn-xs more_books" style="float:right;">Դիտել Բոլորը</button></a> 
    </div>
    <div class="left_content_2">
       <h3>Նախընտրած գրքեր</h3>
       <hr>
       <div>
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive" alt="logo">
           <span>
               <p class="book_title"><b><a href="{{ url('/book_page') }}"> Սպիտակ ժանիքը</a></b></p>
               <p class="book_author"> Հեղինակ։<a href="{{ url('/author_page') }}">Ջեկ Լոնդոն</a></p>
           </span>
        </div>
       <div><img src="{{ asset('images/white_fang.png') }}" class="img-responsive" alt="logo"> 
            <span>
               <p class="book_title"><b><a href="{{ url('/book_page') }}"> Սպիտակ ժանիքը</a></b></p>
               <p class="book_author"> Հեղինակ։<a href="{{ url('/author_page') }}">Ջեկ Լոնդոն</a></p>
           </span>
        </div>
       <div><img src="{{ asset('images/white_fang.png') }}" class="img-responsive" alt="logo">
            <span>
               <p class="book_title"><b><a href="{{ url('/book_page') }}"> Սպիտակ ժանիքը</a></b></p>
               <p class="book_author"> Հեղինակ։<a href="{{ url('/author_page') }}">Ջեկ Լոնդոն</a></p>
           </span>
        </div>
        <a href="{{ url('/rated_books') }}"> <button type="button" class="btn btn-default btn-xs more_books" style="float:right;">Դիտել Բոլորը</button></a>
    </div>
    </div>
    <div class="col-sm-10 book_page_right book_page_carusel"  style="margin-top: 313px;width:77%;padding-left: 50px;">
        <div class="row" style="margin-bottom:30px;">
                <h4><b style="border-bottom:3px solid #ff6600;padding-bottom:5px;">Ձեզ կարող է հետաքրքրել նաև</b></h4><hr> 
    <div class="center" style="max-width:762px;margin:0 auto;">
                                <div class="books">
                                   <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="img">
                                    <div>
                                        <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                                        <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                                         <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="img"  width="70px">
                                        <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
                                    </div>
                                 </div>  
                                 <div class="books">
                                   <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="img">
                                    <div>
                                        <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                                        <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                                         <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="img"  width="70px">
                                        <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
                                    </div>
                                 </div>  
                                 <div class="books">
                                   <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="img">
                                    <div>
                                        <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                                        <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                                         <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="img"  width="70px">
                                        <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
                                    </div>
                                 </div>  
                                 <div class="books">
                                   <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="img">
                                    <div>
                                        <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                                        <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                                         <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="img"  width="70px">
                                        <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
                                    </div>
                                 </div>  
                                 <div class="books">
                                   <img src="{{ asset('images/white_fang.png') }}" class="img-responsive book_img" alt="img">
                                    <div>
                                        <p class="book_title"><b>Սպիտակ ժանիքը</b></p>
                                        <p class="book_author">Հեղինակ։Ջեկ Լոնդոն</p>
                                         <img src="{{ asset('images/star4.png') }}" class="img-responsive" alt="img"  width="70px">
                                        <a href="{{ url('/book_page') }}" > <button type="button" class="btn btn-default">Իմանալ Ավելին</button></a>
                                    </div>
                                 </div>  


      </div>
        </div>
        <div class="row">
       <h4><b style="border-bottom:3px solid #ff6600;padding-bottom:5px;margin-top:50px;">Կարծիքներ</b></h4><hr>
            
       <div class="fb-comments" data-href="http://gogotravel.smarts.am/personal_page" data-width="100%" data-numposts="5"></div>  

        </div>    

    </div>
    </div>
</div>
</section>    

@endsection
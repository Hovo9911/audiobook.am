@extends('layouts.app')

@section('content')
<section class="page_content">
<div class="container">

    <div class="row">
    <div class="col-sm-2 left_content" style="width:23%">
    <div class="left_content_2" style="margin-top:0;">
       <h3>Նախընտրած գրքեր</h3>
       <hr>
       <div>
           <img src="{{ asset('images/white_fang.png') }}" class="img-responsive" alt="logo">
           <span>
               <p class="book_title"><b><a href="{{ url('/book_page') }}"> Սպիտակ ժանիքը</a></b></p>
               <p class="book_author"> Հեղինակ։<a href="{{ url('/author_page') }}">Ջեկ Լոնդոն</a></p>
           </span>
        </div>
       <div><img src="{{ asset('images/white_fang.png') }}" class="img-responsive" alt="logo"> 
            <span>
               <p class="book_title"><b><a href="{{ url('/book_page') }}"> Սպիտակ ժանիքը</a></b></p>
               <p class="book_author"> Հեղինակ։<a href="{{ url('/author_page') }}">Ջեկ Լոնդոն</a></p>
           </span>
        </div>
       <div><img src="{{ asset('images/white_fang.png') }}" class="img-responsive" alt="logo">
            <span>
               <p class="book_title"><b><a href="{{ url('/book_page') }}"> Սպիտակ ժանիքը</a></b></p>
               <p class="book_author"> Հեղինակ։<a href="{{ url('/author_page') }}">Ջեկ Լոնդոն</a></p>
           </span>
        </div>
        <a href="{{ url('/rated_books') }}"> <button type="button" class="btn btn-default btn-xs more_books" style="float:right;">Դիտել Բոլորը</button></a>
    </div>
    </div>
    <div class="col-sm-10 book_page_right"  style="width:77%;padding: 0 50px;">
    <h4 class="page_title"><b>Սիրված հեղինակներ</b></h4>
      <div class="row center_content">
      	@foreach($authors as $author)
       <div class="col-sm-4">
         <div class="books">
           <img src="{{ $author->author_image }}" class="img-responsive book_img" alt="logo">
            <div>
                <p class="book_title"><b>{{ $author->author_name }}</b></p>
                 <img src="{{ asset('images/star5.png') }}" class="img-responsive" alt="logo"  width="70px">
                 <form action="/author_page1" method="post">
                 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
                 	<input type="hidden" name="author_id" value="{{ $author->id}}">
                    <button type="submit" class="btn btn-default">Իմանալ Ավելին</button>
                </form>
            </div>
         </div>
        </div>
       @endforeach
       </div>  
        
        <nav aria-label="navigation" style="text-align:center;">
          <ul class="pagination">
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
              </a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
              <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
              </a>
            </li>
          </ul>
        </nav>
        
     </div>
    </div>
</div>
</section>	   	   
@endsection
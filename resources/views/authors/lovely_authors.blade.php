@extends('layouts.app')

@section('content')

    <section class="page_content">
        <div class="container">

            <div class="row">
                <div class="col-sm-2 left_content" style="width:23%">
                    <div class="left_content_2" style="margin-top:0;">
                        @include('layouts.lovely_books')
                    </div>
                </div>
                <div class="col-sm-10 book_page_right"  style="width:77%;padding: 0 50px;">
                    <div style="position:relative;">
                        <h4 class="page_title"><b>Սիրված հեղինակներ</b></h4>

                    </div>
                    <div class="row center_content author_content">
                        @foreach($lovely_authors as $author)
                            <div class="col-sm-4">
                                <div class="books autors">
                                    <img src="{{ $author->author_image }}" class="img-responsive book_img" alt="logo">
                                    <div class="books_info">
                                        <p class="book_title"><b>{{ $author->author_name }}</b></p>
                                    </div>
                                    <a href="{{ url('/author_page/'.$author->href) }}">
                                        <button type="submit" class="btn btn-default">Իմանալ Ավելին</button>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <nav aria-label="navigation" style="text-align:center;">
                        <ul class="pagination">
                            <li class="page-item">
                            </li>
                            {{ $lovely_authors->links() }}
                            <li class="page-item">
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>

@endsection


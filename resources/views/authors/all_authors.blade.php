@extends('layouts.app')

@section('content')
<section class="page_content">
<div class="container">

    <div class="row">
    <div class="col-sm-2 left_content" style="width:23%">
    <div class="left_content_2" style="margin-top:0;">
      @include('layouts.lovely_books')
    </div>
    </div>
    <div class="col-sm-10 book_page_right"  style="width:77%;padding: 0 50px;">
    <div style="position:relative;">   
    <h4 class="page_title"><b>Բոլոր հեղինակները</b></h4>
            <div class="input-group suggest" style="position: absolute;right: 25px;top: 5px;">  
            <form id="author_search_form" method="GET" action="">
             <input type="text" value="<?=(!empty($_GET['author_name'])) ? $_GET['author_name'] :'' ?>" class="form-control authorsAutocomplete" name="author_name"  id="author_name">
             <span style="display:none"></span>
             <span style="display:none"></span>
             <span style="display:none"></span> 

              <span class="input-group-btn" style="border-radius: 25px;display:inherit">
                <button class="btn btn-secondary" type="sumbit"><i class="glyphicon glyphicon-search" style="margin-right: 5px;"></i></button>
              </span>
            </form>
            </div>  
    </div>    
      <div class="row center_content author_content">
        @foreach($authors as $author)
       <div class="col-sm-4">
         <div class="books autors">
           <img src="{{ $author->author_image }}" class="img-responsive book_img" alt="logo">
            <div class="books_info">
                <p class="book_title"><b>{{ $author->author_name }}</b></p>
            </div>
              <a href="{{ url('/author_page/'.$author->href) }}">
                <button type="submit" class="btn btn-default">Իմանալ Ավելին</button>
              </a>
         </div>
        </div>
       @endforeach
       </div>        
        <nav aria-label="navigation" style="text-align:center;">
          <ul class="pagination">
            <li class="page-item">
                </li>
                 {{ $authors->links() }}
                <li class="page-item">
            </li>
          </ul>
        </nav> 
     </div>
    </div>
</div>
</section>
@section('scripts')
<script src="{{ asset('js/author.js') }}"></script>
 @endsection
@endsection
@extends('layouts.app')

@section('content')
<section class="book_page_banner">
<div class="container">
<div class="row">
  <div class="col-sm-3"></div>  
  <div class="col-sm-8">
        <div class="row banner_content author-content">
          @foreach($authors as $author)
        <div class="col-sm-5" style="text-align:center;margin-top:70px;">
            <img src="{{ $author->author_image }}" alt="img" style="width:260px;box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.2);">
        </div>
        <div class="col-sm-7 banner_text" style="margin-top:70px;">
        <p class="book_title show-page" style="font-size:25px"><b>{{ $author->author_name}}</b></p>
            
         
            <p class="audio_book_text" >{{ $author->author_biography}}</p>
        </div>
        @endforeach
        </div>  
   </div>  
</div>
</div>
</section>

<section class="page_content">
    
<div class="container">
    <div class="row">
    <div class="col-sm-2 left_content" style="width:23%">
    <div class="left_content_1">
      @include('layouts.lovely_authors')
    </div>
    <div class="left_content_2">
      @include('layouts.lovely_books')
    </div>
    </div>
      @include('layouts.author_books')
    </div>
</div>
    
</section>
@endsection
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWishList
 * @package App
 */
class ExchangeRates extends Model
{
    /**
     * @var string
     */
    protected $table = 'exchange_rates';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'currency','rate','date'
    ];






}

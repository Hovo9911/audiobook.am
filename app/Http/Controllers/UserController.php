<?php

namespace App\Http\Controllers;

use App\BookBilling;
use App\Books;
use App\User;
use App\UserWishList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

/**
 * Class ConfirmationController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * method AddWishList
     *
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function AddWishList(Request $request)
    {
        if ($request->ajax()) {

            $this->validate($request, [
                'book_id' => 'required'
            ]);

            $book_id = $request['book_id'];

            if(!is_null(Books::where(['book_id'=>$book_id])->first())){

                $check_book = UserWishList::where([
                    'book_id' => $book_id,
                    'user_id' => Auth::id()
                ])->first();

                if (is_null($check_book)) {
                    UserWishList::create([
                        'book_id' => $book_id,
                        'user_id' => Auth::id()
                    ]);
                }

                return response()->json([
                    'status' => 'success'
                ]);
            }
        }

        return false;
    }

    /**
     * method PersonalPage
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function PersonalPage(){

        $favorite_books = UserWishList::with(['book'=>function($q){
            $q->with(['author']);
        }])->where(['user_id'=>Auth::id()])->orderBy('id', 'desc')->get();

        $purchased_books = BookBilling::with(['book'=>function($q){
            $q->with(['author','records'])->get();
        }])->where(['user_id'=>Auth::id(),'payment_status'=>'success'])->orderBy('updated_at', 'desc')->get();
        if(Auth::user()->id == 5 or Auth::user()->id == 97){
            $all_trying_to_purchase_books = BookBilling::with(['buyer','book'=>function($q){
                $q->with(['author','records'])->get();
            }])->where('user_id','!=',5)->orderBy('created_at', 'desc')->get();
            return view('user.personal_page',compact('favorite_books','purchased_books','all_trying_to_purchase_books'));
        }else{

        return view('user.personal_page',compact('favorite_books','purchased_books'));
        }
    }

    /**
     * method DestroyWishList
     *
     * @param $book_id
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function DestroyWishList($book_id,Request $request){
        if ($request->ajax()) {
            $book = UserWishList::where(['user_id' => Auth::id(), 'book_id' => $book_id])->first();

            $status = 'error';

            if (!is_null($book)) {
                $book->delete();
                $status = 'success';
            }

            return response()->json([
                'status'=>$status
            ]);
        }
        return false;
    }

    /**
     * method PersonalInfoUpdate
     *
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function PersonalInfoUpdate(Request $request)
    {
        if ($request->ajax()) {

            $this->validate($request,[
               'first_name'=>'required|string|max:255',
               'last_name'=>'required|string|max:255',
            ]);

            User::where(['id'=>Auth::id()])->update([
                'first_name'=>$request['first_name'],
                'last_name'=>$request['last_name'],
                'telephone'=>$request['telephone'],
            ]);

            return response()->json([
                'status'=>'success'
            ]);
        }

        return false;
    }

    /**
     * ContactUs
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ContactUs(Request $request){

        $this->validate($request, [
            'message' => 'required|max:5000',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ]);

        $data = $request->all();

        $file = $request->file('file');

        // Send Contact Us
        Mail::send('mail-templates.contact', ['data' => $data], function ($m) use ($data,$file) {
            $m->from($data['email'], $data['subject']);

            if(!empty($file)){
                $m->attach($file,  $options = [
                    'as' => $file->getClientOriginalName(), // If you want you can chnage original name to custom name
                    'mime' => $file->getMimeType()
                ]);
            }

            $m->to('stepan.anisonyan@smarts.am')->subject($data['subject']);
        });

        if (count(Mail::failures()) > 0) {
            return back()->with(['error' => 'Please Try later.']);
        }

        return back()->with(['success' => 'Ձեր հաղորդագրությունը հաջողությամբ ուղարկվել է:']);
    }
}

<?php

namespace App\Http\Controllers\PayPal;

use App\BookBilling;
use App\Books;
use App\ExchangeRates;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use PayPal\Api\InputFields;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Presentation;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\Amount;
use PayPal\Api\WebProfile;
use Illuminate\Support\Facades\Mail;

/**
 * Class PaymentsController
 * @package App\Http\Controllers
 */
class PayPalController extends Controller
{
    const CLIENT_ID = 'ASsJwYRu1PqnC-Xjs3J8_x6scyMShNEeRt3bv6cqcSfsg5wC-89T-YlKa9BRlDezh5_uRQYVOa7ZEMKw';
    const CLIENT_SECRET = 'EKT0LD0-dcby1p0Jf7K1R8eG42dzXaoewKRU9ppfGWBgV6qmwJaKM3Q_rWv57uYLsHyq0PcyYJZ6zikh';

    /**
     * PayPalManualPayment
     *
     * @param $price
     * @return \Illuminate\Http\RedirectResponse|null|string
     */
    public function PayPalManualPayment($price,$book_id){

        $date = date('Y-m-d');
        $rate = ExchangeRates::where(['date'=>$date,"currency" => "EURO"])->pluck('rate')->first();

        $book = Books::where(['book_id'=>$book_id])->first();

        if(is_null($book)){
            return back()->with('error','System Error (118)');
        }

        if(is_null($rate)){
            $rate = ExchangeRates::where(["currency" => "EURO"])->pluck('rate')->first();
        }

        $price = number_format($book->book_price / $rate,2);

        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                self::CLIENT_ID,
                self::CLIENT_SECRET
            )
        );

       $apiContext->setConfig(
            array(
                'mode' => 'live'
            )
        );


        $item1 =  new Item();
        $item1->setName('Book purchased')
            ->setCurrency('EUR')
            ->setQuantity('1')
            ->setPrice($price);

        $itemList = new ItemList();
        $itemList->setItems(array($item1));



        $presentation = new Presentation();
        $presentation->setLogoImage("http://audiobook.am/images/logo-130.png")
            ->setBrandName("audiobook.am");
        $inputFields = new InputFields();
        $inputFields->setAllowNote(true)
            ->setNoShipping(1)
            ->setAddressOverride(0);
        $webProfile = new WebProfile();
        $webProfile->setName(" audiobook". uniqid())
            ->setPresentation($presentation);

        try {

            $createdProfile = $webProfile->create($apiContext);
            $createdProfileID = json_decode($createdProfile);
            $profileid = $createdProfileID->id;

        } catch(\PayPal\Exception\PayPalConnectionException $pce) {

            $url = url('payment/cancel');

            Session::flash('error','ՍԽԱԼ (223)');

            return $url;
        }

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");


        // Set redirect urls
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url('paypal/confirm/'.$book_id ))
            ->setCancelUrl(url('/payment/cancel'));

        // Set payment amount
        $amount = new Amount();
        $amount->setCurrency("EUR")
            ->setTotal($price);

        // Set transaction object
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setDescription("Payment description")
            ->setItemList($itemList);

        // Create the full payment object
        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction))
        ->setExperienceProfileId($profileid);

        try {
            $payment->create($apiContext);

            // Get PayPal redirect URL and redirect user
            $approvalUrl = $payment->getApprovalLink();

            if(is_null( BookBilling::where(['book_id'=>$book_id,'user_id'=>Auth::id(),'payment_type'=>'paypal'])->first())){

                BookBilling::create([
                    'book_id'=>$book_id,
                    'user_id'=>Auth::id(),
                    'payment_type'=>'paypal',
                    'created_at'=>Carbon::now(),
                    'price'=>@$price,
                ]);

            }else{

                BookBilling::where(['book_id'=>$book_id,'user_id'=>Auth::id(),'payment_type'=>'paypal'])->update([
                   'updated_at'=> Carbon::now()
                ]);
            }



            return $approvalUrl;

            // REDIRECT USER TO $approvalUrl
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

            $url = url('payment/cancel');

            Session::flash('error','ՍԽԱԼ (111)');

            return $url;

        } catch (\Exception $ex) {

            $url = url('payment/cancel');

            Session::flash('error','ՍԽԱԼ (222)');

            return $url;
        }

    }

    /**
     * method PayPalManualConfirm
     *
     * @param @id
     * @return Redirect
     */
    public function PayPalManualConfirm($book_id)
    {
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                self::CLIENT_ID,
                self::CLIENT_SECRET
            )
        );

        $apiContext->setConfig(
            array(
                'mode' => 'live'
            )
        );

        // Get payment object by passing paymentId
        $paymentId = $_GET['paymentId'];
        $payment = Payment::get($paymentId, $apiContext);
        $payerId = $_GET['PayerID'];

        // Execute payment with payer id
        $execution = new \PayPal\Api\PaymentExecution();
        $execution->setPayerId($payerId);

        try {
            // Execute payment
            $payment->execute($execution, $apiContext);


            BookBilling::where(['book_id'=>$book_id,'user_id'=>Auth::id(),'payment_type'=>'paypal'])->update([
                'payment_status'=>'success',
                'payment_token'=>$_GET['token']
            ]);

            $book = Books::where(['book_id'=>$book_id])->first();

            $data['subject'] = 'Payment successfully processed!';
            $data['from-email'] = 'info@audiobook.am';
            $data['email'] = Auth::user()->email;
            $data['first_name'] =  Auth::user()->first_name;
            $data['last_name'] =  Auth::user()->last_name;
            $data['book_name'] = $book->book_title;

            Mail::send('mail-templates.buy-book', ['data' => $data], function ($m) use ($data) {
                $m->from($data['from-email'], 'Audiobook.am');
                $m->to($data['email'])->subject($data['subject']);
            });

            return redirect('payment/success');

        } catch (\PayPal\Exception\PayPalConnectionException $ex) {

            return redirect('payment/cancel')->with(['error' => 'ՍԽԱԼ (333)']);

        } catch (\Exception $ex) {
            return redirect('payment/cancel')->with(['error' => 'ՍԽԱԼ (444)']);
        }

    }


}
<?php

namespace App\Http\Controllers;


use App\Authors;
use App\Books;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(){
        $new_authors                     = Authors::orderBy('auth_id', 'desc')->take(3)->get();
        $lovely_books                    = Books::lovely_books();
        $slideshow_books                 = Books::slideshow_books();
        $slideshow_new_added_books       = Books::slideshow_new_added_books();
        $slideshow_most_listened_books   = Books::slideshow_most_listened_books();
        $slideshow_top_rated_books       = Books::slideshow_top_rated_books();
        return view('/home',['new_authors' => $new_authors,'lovely_books' => $lovely_books,'slideshow_books' => $slideshow_books,'slideshow_new_added_books' => $slideshow_new_added_books ,'slideshow_most_listened_books' => $slideshow_most_listened_books,'slideshow_top_rated_books' => $slideshow_top_rated_books]);
     }

}

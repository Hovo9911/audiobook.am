<?php

namespace App\Http\Controllers;

use App\Books;
use App\Http\Controllers\PayPal\PayPalController;
use App\Http\Controllers\Ameria\AmeriaController;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

/**
 * Class CheckoutController
 * @package App\Http\Controllers
 */
class CheckoutController extends Controller
{
    /**
     * @return mixed
     */
    public function store(Request $request,PayPalController $paypal,AmeriaController $ameria)
    {
        $this->validate($request,[
           'pay_type'=>'required',
           'book_id'=>'required',
           'read-accept'=>'required'
        ]);

        $pay_type = $request['pay_type'];
        $book_id = $request['book_id'];
        $book = Books::where(['book_id'=>$book_id])->first();
        $amount = $book->book_price;

        if(!is_null($book)){

            if($pay_type == 'paypal'){
//                $amount = "2.20";
                $redirect_url = $paypal->PayPalManualPayment($amount,$book->book_id);
                return redirect($redirect_url);
            }elseif($pay_type == 'credit_card'){
//                $amount = "10.00";
                $redirect_url = $ameria->AmeriaGetPaymentID($amount,$book->book_id);
            }

        }

        // return back()->with(['error'=>'Error']);
    }

}
<?php

namespace App\Http\Controllers;

use App\BookBilling;
use App\BookCategories;
use App\BookRates;
use App\UserWishList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\ResponseFactory;
use App\Books;
use App\Authors;
use App\Mp3Cutter\PHPMP3;

/**
 * Class BookCategoriesController
 * @package App\Http\Controllers
 */
class BookCategoriesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $books_categories = DB::table('books_categories')->get();
        $lovely_authors = Authors::lovely_authors();
        $lovely_books = Books::lovely_books();

        if ($request->book_name != null || $request->book_category != null) {

            $books = DB::table('books')
                ->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id');


            if ($request->book_name != null){
                $books->where('books.book_title', 'like', '%' . $request->book_name . '%')
                    ->orWhere('books.book_href', 'like', '%' . $request->book_name . '%');
            }

            if($request->book_category != null && $request->book_category != 'all'){
               $category_id = BookCategories::where(['name' =>$request->book_category])->pluck('id')->first();
               $books->where(['book_category'=>$category_id]);
            }

            $books = $books->paginate(12);


            return view('books.books', ['books' => $books, 'books_categories' => $books_categories, 'lovely_authors' => $lovely_authors, 'lovely_books' => $lovely_books, 'search' => 'yes']);

        } else {


            $books = DB::table('books')->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')->paginate(12);


            return view('books.books', ['books' => $books, 'books_categories' => $books_categories, 'lovely_authors' => $lovely_authors, 'lovely_books' => $lovely_books, 'search' => 'no']);

        }

    }

    /**
     * show_book_page
     *
     * @param $href
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_book_page($href)
    {

        $books = DB::table('books')
            ->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
            ->where('books.book_href', '=', $href)->get();


        foreach ($books as $book) {
            $slideshow_may_interested_books = DB::table('books')->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
                ->where('books.book_id', '!=', $book->book_id)
                ->orderByRaw('books.author_id = ? desc', [$book->author_id])
                ->orderByRaw('books.book_category = ? desc', [$book->book_category])
                ->limit('5')->get();

        }

        $lovely_authors = Authors::lovely_authors();
        $lovely_books = Books::lovely_books();
        $user_wish_list_books = UserWishList::where(['user_id' => Auth::id()])->pluck('book_id')->toArray();

        $rate = BookRates::where([ 'book_id'=>$books[0]->book_id])->avg('rate');

        $average_rate = round(2*$rate)/2;
        $user_rated = false;

        $purchased_book = false;

        if(Auth::check() && !is_null(BookBilling::where(['book_id'=>$books[0]->book_id,'user_id'=>Auth::id(),'payment_status'=>'success'])->first())){
            $purchased_book = true;
        }

        if(!is_null(BookRates::where([ 'book_id'=>$books[0]->book_id,'user_id'=>Auth::id()])->first())){
            $user_rated = true;
        }


        return view('books.book_page', ['purchased_book'=>$purchased_book,'user_rated'=>$user_rated,'average_rate'=>$average_rate,'books' => $books, 'lovely_authors' => $lovely_authors, 'lovely_books' => $lovely_books, 'slideshow_may_interested_books' => $slideshow_may_interested_books, 'user_wish_list_books' => $user_wish_list_books]);

    }

    /**
     * show_book_records
     *
     * @param $book_id
     * @return mixed
     */
    public static function show_book_records($book_id)
    {

        $books = DB::table('books_records')
            ->where('books_records.book_id', '=', $book_id)->limit(3)->get();

        foreach ($books as $book) {

            $full_book_mp3_path = storage_path('uploads/tmp/' . $book->book_record . '.mp3');
            $cut_book_mp3_path = public_path('uploads/tmp/' . $book->book_record . '.mp3');

            if (!file_exists($cut_book_mp3_path)) {

                if (file_exists($full_book_mp3_path)) {



                    $folder_name = explode('/',$book->book_record)[0];

                    if(!is_dir(public_path('uploads/tmp/'.$folder_name ))){

                        mkdir(public_path('uploads/tmp/'.$folder_name), 0777, true);
                    }



                    $mp3 = new PHPMP3($full_book_mp3_path);

                    $mp3_1 = $mp3->extract(0, 50);
                    $mp3_1->save($cut_book_mp3_path);

                }

            }
        }

        return $books;

    }

    /**
     * truncate
     *
     * @param $text
     * @param $chars
     * @return bool|string
     */
    public static function truncate($text, $chars)
    {
        if($text == 'Աշխարհայեցողութիւն'){

            $text = substr($text, 0, $chars);
            return $text = $text . "...";
        }else{
            if (strlen($text) > $chars) {
                $text = $text . " ";
                $text = substr($text, 0, $chars);
                $text = substr($text, 0, strrpos($text, ' '));
                return $text = $text . "...";
            } else {
                return $text;
            }
        }



    }

    /**
     * new_added_books
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function new_added_books()
    {

        $new_added_books = Books::new_added_books();
        $lovely_authors = Authors::lovely_authors();
        $lovely_books = Books::lovely_books();
        return view('books.new_added_books', ['new_added_books' => $new_added_books, 'lovely_authors' => $lovely_authors, 'lovely_books' => $lovely_books]);

    }

    /**
     * most_listened_books
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function most_listened_books()
    {

        $most_listened_books = Books::most_listened_books();
        $lovely_authors = Authors::lovely_authors();
        $lovely_books = Books::lovely_books();



        return view('books.most_listened_books', ['most_listened_books' => $most_listened_books, 'lovely_authors' => $lovely_authors, 'lovely_books' => $lovely_books]);

    }

    /**
     * top_rated_books
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function top_rated_books()
    {

        $top_rated_books = Books::top_rated_books();
        $lovely_authors = Authors::lovely_authors();
        $lovely_books = Books::lovely_books();
        return view('books.top_rated_books', ['top_rated_books' => $top_rated_books, 'lovely_authors' => $lovely_authors, 'lovely_books' => $lovely_books]);

    }

    /**
     * favourite_books
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function favourite_books()
    {

        $favourite_books = DB::table('books')
            ->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
            ->orderByRaw("RAND()")
            ->paginate(12);

        $lovely_authors = Authors::lovely_authors();
        $lovely_books = Books::lovely_books();
        return view('books.favourite_books', ['favourite_books' => $favourite_books, 'lovely_authors' => $lovely_authors, 'lovely_books' => $lovely_books]);

    }

    /**
     * BookRate
     *
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function BookRate(Request $request){

        if ($request->ajax()) {

            $rate = $request['rating'];
            $book_id = $request['book_id'];

            if((!is_null(Books::where(['book_id'=>$book_id])->first()) && $rate <= 5) && is_null(BookRates::where(['book_id'=>$book_id,  'user_id'=>Auth::id()])->first())){

                BookRates::create([
                    'book_id'=>$book_id,
                    'rate'=>$rate,
                    'user_id'=>Auth::id()
                ]);

                $rate = BookRates::where([ 'book_id'=>$book_id])->avg('rate');

                $rate = round(2*$rate)/2;

                Books::where([ 'book_id'=>$book_id])->update(['book_average_rating'=>$rate]);


                return response()->json([
                    'status'=>'success',
                    'rate'=>$rate
                ]);

            }
        }
        return false;

    }

    /**
     * AddPlayTime
     *
     * @param $times
     * @return string
     */
    public static function AddPlayTime($times) {
        $minutes = 0; //declare minutes either it gives Notice: Undefined variable
        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;

        // returns the time already formatted
        return sprintf('%02d:%02d', $hours, $minutes);
    }

}

<?php

namespace App\Http\Controllers\Ameria;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\BookBilling;
use App\Books;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use SoapClient;
use DB;
use Illuminate\Support\Facades\Mail;


class AmeriaController extends Controller
{
    //Connection Info

    //Test Info
//    private $ameriaClient;
//    private $ameriaClientID = 'aafe66cd-f63d-4b39-8891-05c1200b1471';
//    private $ameriaUsername = '3d19541048';
//    private $ameriaPassword = 'lazY2k';

    //Live Info
    private $ameriaClient;
    private $ameriaClientID = '98f386fe-d9fa-4881-9011-760ee2152c57';
    private $ameriaUsername = '19533665_api';
    private $ameriaPassword = '723zD57U78mM84t';

    public function __construct(Request $request)    
    {    
		$options = array( 
							'soap_version'    => SOAP_1_1, 
							'exceptions'      => true, 
							'trace'           => 1, 
							'wdsl_local_copy' => true
						);

		$this->ameriaClient = new SoapClient("https://payments.ameriabank.am/webservice/PaymentService.svc?wsdl", $options);
    }


    public function AmeriaGetPaymentID($price,$book_id){ 
      
     
 			$parms['paymentfields']['ClientID']		 =   $this->ameriaClientID; // clientID from Ameriabank
			$parms['paymentfields']['Username']		 =   $this->ameriaUsername;// username from Ameriabank
			$parms['paymentfields']['Password']		 =   $this->ameriaPassword; // password from Ameriabank
			$parms['paymentfields']['OrderID']		 =   rand(); // unique ID for transaction
			$parms['paymentfields']['Description']	 =	 'audiobooks.am book purchase';//$request->Description; // Order Description
			$parms['paymentfields']['PaymentAmount'] =   $price; //$request->PaymentAmount; // payment amount 
			$parms['paymentfields']['backURL']		 =   'http://audiobook.am/confirm_ameria_payment'; // after transaction redirect to this url (example back.php)
			 

			// Generate unique payment ID  

			$webService = $this->ameriaClient->GetPaymentID($parms);

			if($webService->GetPaymentIDResult->Respcode == '1' && $webService->GetPaymentIDResult->Respmessage =='OK')
			{
				//Insert to db
                if(is_null( BookBilling::where(['book_id'=>$book_id,'user_id'=>Auth::id(), 'payment_type'=>'credit_card'])->first())){

                    BookBilling::create([
                        'book_id'     =>$book_id,
                        'user_id'     =>Auth::id(),
                        'payment_type'=>'credit_card',
                        'payment_id'  =>$webService->GetPaymentIDResult->PaymentID,
                        'order_id'    =>$parms['paymentfields']['OrderID'],
                        'created_at'  =>Carbon::now()
                    ]);


                }else{


                    BookBilling::where(['book_id'=>$book_id,'user_id'=>Auth::id(), 'payment_type'=>'credit_card'])->update([
                        'updated_at'=> Carbon::now(),
                        'payment_id'  =>$webService->GetPaymentIDResult->PaymentID,
                        'order_id'    =>$parms['paymentfields']['OrderID']
                    ]);

                }


                //Redirect to Ameriabank server

	            echo "<script type='text/javascript'>\n";
	            echo "window.location.replace('https://payments.ameriabank.am/forms/frm_paymentstype.aspx?lang=am&paymentid=".$webService->GetPaymentIDResult->PaymentID."');\n";
	            echo "</script>";


			}
			else
			{
				// Error Message 
			}

    }

    public function AmeriaGetPaymentFields(Request $request){

//        dd($request);

        $book_id = BookBilling::where(['order_id'=>$request->orderID,'user_id'=>Auth::id()])->first()->book_id;
//        dd($book_id);
        $amount  = Books::where(['book_id'=>$book_id])->first()->book_price;
//        $amount  = "10.00";

	    $parms['paymentfields']['ClientID']		 =   $this->ameriaClientID;  // clientID from Ameriabank
		$parms['paymentfields']['Username']		 =   $this->ameriaUsername;  // username from Ameriabank
		$parms['paymentfields']['Password']		 =   $this->ameriaPassword;  // password from Ameriabank
		$parms['paymentfields']['OrderID']		 =   $request->orderID;      // unique ID for transaction
		$parms['paymentfields']['PaymentAmount'] =	 $amount;// payment amount 
 
        // Get payment information
        
        $webService = $this->ameriaClient->GetPaymentFields($parms);
//dd($webService);
        DB::table('ameria_payment_info')->insert([
                'order_id'      => $webService->GetPaymentFieldsResult->orderid, 
                'amount'        => $webService->GetPaymentFieldsResult->amount,
                'approveamount' => $webService->GetPaymentFieldsResult->approveamount, 
                'authcode'      => $webService->GetPaymentFieldsResult->authcode,
                'cardnumber'    => $webService->GetPaymentFieldsResult->cardnumber, 
                'clientname'    => $webService->GetPaymentFieldsResult->clientname,
                'currency'      => $webService->GetPaymentFieldsResult->currency, 
                'datetime'      => $webService->GetPaymentFieldsResult->datetime,
                'depositamount' => $webService->GetPaymentFieldsResult->depositamount, 
                'descr'         => $webService->GetPaymentFieldsResult->descr,
                'merchantid'    => $webService->GetPaymentFieldsResult->merchantid, 
                'opaque'        => $webService->GetPaymentFieldsResult->opaque,
                'payment_state' => $webService->GetPaymentFieldsResult->payment_state, 
                'respcode'      => $webService->GetPaymentFieldsResult->respcode, 
                'rrn'           => $webService->GetPaymentFieldsResult->rrn,
                'stan'          => $webService->GetPaymentFieldsResult->stan, 
                'terminalid'    => $webService->GetPaymentFieldsResult->terminalid,
                'trxnDetails'   => $webService->GetPaymentFieldsResult->trxnDetails, 
                'created_at'    => Carbon::now()

            ]);
        
        
        if($webService->GetPaymentFieldsResult->respcode == '00')
        {
            BookBilling::where(['book_id'=>$book_id,'user_id'=>Auth::id(),'payment_type'=>'credit_card'])->update([
                'payment_status'=>'success'
            ]);


            $book = Books::where(['book_id'=>$book_id])->first();

            $data['subject'] = 'Book Audiobook';
            $data['from-email'] = 'info@audiobook.am';
            $data['email'] = Auth::user()->email;
            $data['first_name'] =  Auth::user()->first_name;
            $data['last_name'] =  Auth::user()->last_name;
            $data['book_name'] = $book->book_title;

            Mail::send('mail-templates.buy-book', ['data' => $data], function ($m) use ($data) {
                $m->from($data['from-email'], 'Audiobook.am');
                $m->to($data['email'])->subject($data['subject']);
            });
            
        	return view('ameria.success');
        }
        else
        {
        	return view('ameria.cancel')->with(['error' => $webService->GetPaymentFieldsResult->descr]);
        
        }      
    }
}

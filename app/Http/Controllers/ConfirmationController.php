<?php

namespace App\Http\Controllers;

use App\BookBilling;
use App\ExchangeRates;
use Illuminate\Http\Request;
use App\Books;
use App\Authors;
use Illuminate\Support\Facades\Auth;

/**
 * Class ConfirmationController
 * @package App\Http\Controllers
 */
class ConfirmationController extends Controller
{
    /**
     * @param bool $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index($id = false, Request $request)
    {

//        dd($request);
        if (!$id) {
            return url('/');
        }

        if(!is_null(BookBilling::where(['book_id'=>$id,'user_id'=>Auth::id(),'payment_status'=>'success'])->first())){
            return redirect('/')->with(['error'=>'Արդեն գնված է']);
        }

        $books_info = Books::where('book_id', '=', $id)
            ->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
            ->get();

        if (count($books_info) == 0) {
            return back();
        }

        $lovely_authors = Authors::lovely_authors();
        $lovely_books = Books::lovely_books();

        $date = date('Y-m-d');
        $currency = ExchangeRates::where(['date'=>$date,"currency" => "EURO"])->first();

        if(is_null($currency)){
            $currency = ExchangeRates::where(["currency" => "EURO"])->first();
        }



        return view('confirmation.confirmation', ['books_info' => $books_info, 'lovely_authors' => $lovely_authors, 'lovely_books' => $lovely_books,'currency'=>$currency]);
    }
}

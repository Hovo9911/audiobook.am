<?php

namespace App\Http\Controllers\Auth;

use App\BookBilling;
use App\BookRates;
use App\Http\Controllers\Controller;
use App\UserWishList;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/personal_page';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    
    protected function authenticated(Request $request, $user){
        if ($request->ajax()){

            $menu = view('layouts/header-menu-user-name')->render();

            $book_wishlist = false;
            $book_rated = false;
            $purchased_book = false;

            if(!empty($request['book'])){
                $book = UserWishList::where(['book_id'=>$request['book'],'user_id'=>$user->id])->first();

                if(!is_null($book)){
                    $book_wishlist = true;
                }

                $book_rate = BookRates::where(['book_id'=>$request['book'],'user_id'=>$user->id])->first();
                if(!is_null($book_rate)){
                    $book_rated = true;
                }

                $purchased_book_check = BookBilling::where(['book_id'=>$request['book'],'user_id'=>$user->id,'payment_status'=>'success'])->first();

                if(!is_null($purchased_book_check)){
                    $purchased_book = true;
                }
            }

            return response()->json([
                'auth' => auth()->check(),
                'menu'=>$menu,
                'rated'=>$book_rated,
                'book'=>$book_wishlist,
                'purchased_book'=>$purchased_book
            ]);

        }
    }



}

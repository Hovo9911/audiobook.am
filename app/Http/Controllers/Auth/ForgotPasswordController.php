<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Notifications\MailResetPasswordToken;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendResetLinkResponse($response)
    {

        return response()->json([
            'status' => 'success',
            'message'=>'Մենք Ձեզ ուղարկել ենք նամակ՝ գաղտնաբառը վերականգնելու հրահանգներով:
Խնդրում ենք ստուգել Ձեր էլ.փոստի հասցեն:'
        ]);

    }

    /**
     * @param Request $request
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request,$response){

        return response()->json([
            'status' => 'error',
            'message'=>'Խնդրում ենք լրացնել այն էլ.հասցեն, որն օգտագործել եք մեր կայքում գրանցվելու ժամանակ:'
        ],422);
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getResetToken(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);


        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            return response()->json(['message'=>'Խնդրում ենք լրացնել այն էլ.հասցեն, որն օգտագործել եք մեր կայքում գրանցվելու ժամանակ:'], 400);
        }

        $token = $this->broker()->createToken($user);
//       sendPasswordResetNotification($token);
        \Notification::send($user, new MailResetPasswordToken($token));
        return response()->json(['token' => $token,'url'=> url('password/reset', $token)]);
    }
}

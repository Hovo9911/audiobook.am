<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $data['email'] = $data['email_reg'];
        $data['password'] = $data['password_reg'];
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $data['email'] = $data['email_reg'];
        $data['password'] = $data['password_reg'];

        $data['subject'] = 'Welcome to Audiobook';
        $data['from-email'] = 'info@audiobook.am';

        Mail::send('mail-templates.register', ['data' => $data], function ($m) use ($data) {
            $m->from($data['from-email'], 'Audiobook.am');
            $m->to($data['email'])->subject($data['subject']);
        });

        $mail_send = 1;
        if (count(Mail::failures()) > 0) {
            $mail_send = 0;
        }

        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'telephone' => $data['telephone'],
            'country' => $data['country'],
            'gender' => $data['gender'],
            'password' => bcrypt($data['password']),
            'mail_send'=>$mail_send
        ]);
    }

    /**
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    protected function registered(Request $request, $user)
    {
        if ($request->ajax()){

            $menu = view('layouts/header-menu-user-name')->render();
            return response()->json([
                'auth' => auth()->check(),
                'menu'=>$menu
            ]);

        }
    }
}

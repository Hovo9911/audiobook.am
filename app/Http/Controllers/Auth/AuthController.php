<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();

        // OAuth Two Providers
//        $token = $user->token;
//        $refreshToken = $user->refreshToken; // not always provided
//        $expiresIn = $user->expiresIn;

// OAuth One Providers
//        $token = $user->token;
//        $tokenSecret = $user->tokenSecret;
        $facebook_user_id = $user->getId();

        $name = $user->getName();

        $user_logged = User::where(['facebook_user_id'=>$facebook_user_id])->first();


        if (is_null($user_logged)) {

            $first_name = explode(' ',$name)[0];
            $last_name = explode(' ',$name)[1];

            $user_logged = User::create([
                'facebook_user_id' => $facebook_user_id,
                'first_name' =>$first_name,
                'last_name' =>$last_name,
                'email'=>$user->getEmail().'_'.$facebook_user_id,
            ]);

            $data['subject'] = 'Welcome to Audiobook';
            $data['from-email'] = 'info@audiobook.am';
            $data['email'] = $user->getEmail();
            $data['first_name'] = $first_name;
            $data['last_name'] = $last_name;

            Mail::send('mail-templates.register', ['data' => $data], function ($m) use ($data) {
                $m->from($data['from-email'], 'Audiobook.am');
                $m->to($data['email'])->subject($data['subject']);
            });

            if (count(Mail::failures()) <= 0) {
                User::where(['facebook_user_id'=>$facebook_user_id])->update(['mail_send'=>1]);
            }

        }

        auth()->login($user_logged);

        return redirect('/');


/*      $user->getId();
        $user->getNickname();
        $user->getEmail();
        $user->getAvatar();*/


    }
}
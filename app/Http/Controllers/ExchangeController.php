<?php

namespace App\Http\Controllers;


use App\Authors;
use App\Books;
use App\ExchangeRates;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class ExchangeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * method GetCbExchange
     */
     public function GetCbExchange(){

         $euro =@json_decode(file_get_contents('http://cb.am/latest.json.php?currency=EUR'))->EUR;
         $usd = @json_decode(file_get_contents('http://cb.am/latest.json.php?currency=USD'))->USD ;

         $today = date('Y-m-d');

         $data_euro = [
            'currency'=>"EURO",
            'rate'=>$euro,
            'date'=>$today,
         ];

         $data_usd = [
            'currency'=>"USD",
            'rate'=>$usd,
            'date'=>$today,
         ];

         ExchangeRates::updateOrCreate(['date'=>$today,'currency'=>'EURO'],$data_euro);
         ExchangeRates::updateOrCreate(['date'=>$today,'currency'=>'USD'],$data_usd);
     }

}

<?php

namespace App\Http\Controllers\Admin;

use App\BookCategories;
use App\BookRecords;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\ResponseFactory;
use App\Authors;
use App\Books;

/**
 * Class AuthorController
 * @package App\Http\Controllers
 */
class BookController extends Controller
{


    public function index(){
//        dd('a');
    }

    /**
     * Create
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){
        $authors = Authors::all();
        $categories = BookCategories::all();
        return view('admin.book.create',compact('authors','categories'));
    }

    public function store(Request $request)
    {
        $file = $request->file('book_image');

//        $destinationPath = public_path().'/images/books';
//
//        if($request->hasFile('book_image'))
//        {
//            $img_filename = $file->getClientOriginalName();
//
//            $file->move($destinationPath, $img_filename);
//        }

        $data = $request->all();

        $data['book_image'] = '/images/books/'.$file->getClientOriginalName();

        $book = Books::create($data);

        BookRecords::create([
           'book_id'=>$book->id,
           'book_record_title'=>$data['book_record_title'],
           'book_record_duration'=>$data['book_record_duration'],
           'book_record'=>$data['book_record'],
        ]);

        return back()->with('success','Successfully Created');

    }
}
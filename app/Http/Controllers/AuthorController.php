<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\ResponseFactory;
use App\Authors;
use App\Books;
use Illuminate\Support\Facades\File;

/**
 * Class AuthorController
 * @package App\Http\Controllers
 */
class AuthorController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
//      return response()->download(storage_path("uploads/tmp/charenc/amboxner-xelagarvac.mp3"));

        /*$path = storage_path("uploads/tmp/charenc/amboxner-xelagarvac.mp3");
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = \Illuminate\Support\Facades\Response::make($file, 200);
        $response->header("Content-Type", $type);*/

        if($request->author_name != null && $request->author_name != ''){
            // dd($request);

            $authors = Authors::where('author_name', 'like', '%' . $request->author_name . '%')->orWhere('href', 'like', '%' . $request->author_name . '%')
                ->orderBy('auth_id','DESC')->paginate(12);

            $lovely_books     = Books::lovely_books();
            return view('authors.all_authors',['authors' => $authors,'lovely_books' => $lovely_books,'search' => 'no']);
        }else{
            
            $authors          = Authors::orderBy('auth_id','DESC')->paginate(12);
            $lovely_books     = Books::lovely_books();
            return view('authors.all_authors',['authors' => $authors,'lovely_books' => $lovely_books,'search' => 'yes']);
        }
    }

    /**
     * @param $href
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_author_page($href){

        $authors = DB::table('authors')->where('href', '=', $href)->get();
        foreach ($authors as $author) {
            $author_id = $author->auth_id;
        }
        $lovely_authors   = DB::table('authors')->orderByRaw("RAND()")->limit('3')->get();
        $author_books     = DB::table('books')->where('author_id','=',$author_id)->get();
        $lovely_books     = Books::lovely_books();
    	return view('authors.author_page',['authors' => $authors,'lovely_authors' => $lovely_authors,'author_books' => $author_books,'lovely_books' => $lovely_books]);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get_new_authors(){
    	$new_authors = Authors::orderBy('auth_id', 'desc')->take(3)->get();
    	return view('/',['new_authors' => $new_authors]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function checkAuthorAndBookName(Request $request){
  
        $term = $request->term;      
//        $authors = Authors::where('author_name', 'like', "%$term%")->get();
        $books   = Books::where('book_title', 'like' , "%$term%")->get();
//        foreach ($authors as $author) {
//            $array[] = ['id' => $author->auth_id, 'value' => $author->author_name,'isBook' => 'no'];
//        }

        foreach ($books as $book) {
            $array[] = ['id' => $book->id, 'value' => $book->book_title,'isBook' => 'yes'];
        }

        return \Response::json($array);

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function checkAuthorName(Request $request){
  
        $term = $request->term;      
        $authors = Authors::where('author_name', 'like', "%$term%")->get();

        foreach ($authors as $author) {
            $array[] = ['id' => $author->auth_id, 'value' => $author->author_name,'isBook' => 'no'];
        }

        return \Response::json($array);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function LovelyAuthors(){

        $lovely_authors   = DB::table('authors')->orderByRaw("RAND()")->limit('12')->paginate();
        $lovely_books     = Books::lovely_books();

        return view('authors.lovely_authors',compact('lovely_authors','lovely_books'));

    }

   
}


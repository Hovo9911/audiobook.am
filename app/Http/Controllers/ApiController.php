<?php

namespace App\Http\Controllers;

use App\BookBilling;
use App\BookCategories;
use App\Pages;
use App\User;
use App\UserWishList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\ResponseFactory;
use App\Authors;
use App\Books;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    /**
     * @param $slideshow_id
     * @return mixed
     */
    public function slideshowAPI($slideshow_id)
    {
        switch ($slideshow_id) {
            case 'main_slideshow':
                return \Response::json(Books::slideshow_books());
                break;
            case 'new_books_slideshow':
                return \Response::json(Books::slideshow_new_added_books());
                break;
            case 'most_listened_books_slideshow':
                return \Response::json(Books::slideshow_most_listened_books());
                break;
            case 'top_rated_books_slideshow':
                return \Response::json(Books::slideshow_top_rated_books());
                break;

            default:
                break;
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function allAurhorsAPI(Request $request)
    {
        // dd($request);
        $authors = Authors::orderBy('auth_id', 'DESC')->paginate(12);
        return \Response::json($authors);
    }

    /**
     * @param $href
     * @return mixed
     */
    public function show_author_pageAPI($href)
    {

        $data = array();
        $authors = DB::table('authors')->where('href', '=', $href)->get();
        foreach ($authors as $author) {
            $author_id = $author->auth_id;
        }
        $author_books = DB::table('books')->where('author_id', '=', $author_id)->get();
        $data['author'] = $authors;
        $data['author_books'] = $author_books;
        return \Response::json($data);

    }

    /**
     * @return mixed
     */
    public function new_added_booksAPI()
    {
        return \Response::json(Books::new_added_books());
    }

    /**
     * @return mixed
     */
    public function most_listened_booksAPI()
    {

        return \Response::json(Books::most_listened_books());
    }

    /**
     * @return mixed
     */
    public function top_rated_booksAPI()
    {

        return \Response::json(Books::top_rated_books());
    }

    /**
     * method loginAPI
     *
     * @param Request $request
     * @return string
     */
    public function loginAPI(Request $request)
    {

        $data = $request->all();

        $validator = Validator::make($data, [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {

            return response()->json(['errors' => $validator->errors()],400);
        } else {

            $user = User::where(['email' => $request['email']])->first();

            if (!is_null($user)) {

                if (Hash::check($request['password'], $user->password)) {
                    return response()->json([
                        'status' => 'success',
                        'data' => $user
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'error',
            'message'=>'Մուտքանունը և/կամ գաղտնաբառը սխալ է:'
        ],400);
    }

    /**
     * method allBooksAPI
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function allBooksAPI(Request $request)
    {


        $categories = BookCategories::orderBy('id','DESC')->get();
        if ($request->category_id && $request->category_id != 9) {
            $category_id = $request->category_id;
            $books = Books::with(['author'])->where(['book_category' => $category_id])->paginate(12);
        } else {
            $category_id = 9;
            $books = Books::with(['author'])->paginate(12);
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                'books' => $books,
                'categories' => $categories,
                'category_id' => $category_id
            ]
        ]);
    }

    /**
     * method showBookAPI
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showBookAPI()
    {
        $book_id = $_GET['book_id'];
        $user_id = @$_GET['user_id'];

        if (!empty($book_id)) {
            $book = Books::with(['records'])->where(['book_id' => $book_id])->first();

            foreach ($book->records as $book_record) {
                $times[] = $book_record->book_record_duration;
            }
            $time_sum = BookCategoriesController::AddPlayTime($times);

            $book->mp3_duration = $time_sum;
            $book->wishlist = false;
            $book->purchased = false;

            if (!empty($user_id)) {
                if (UserWishList::where(['book_id' => $book_id, 'user_id' => $user_id])->count() > 0) {
                    $book->wishlist = true;
                }


                if (!is_null(\App\BookBilling::where(['book_id'=>$book_id,'user_id'=>$user_id,'payment_status'=>'success'])->first())) {
                    $book->purchased = true;
                }
            }

            return response()->json([
                'status' => 'success',
                'data' => $book
            ]);
        }
        return response()->json([
            'status' => 'error'
        ],400);
    }

    /**
     * @param $author_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function AuthorBooks($author_id)
    {
        $author = Authors::with(['books'])->where(['auth_id' => $author_id])->first();

        return response()->json([
            'message' => 'success',
            'data' => $author
        ], 401);
    }

    /**
     * method UserPurchasedBooksAPI
     *
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function UserPurchasedBooksAPI($user_id)
    {

        if (!empty($user_id)) {

            $purchased_books = BookBilling::with(['book' => function ($q) {
                $q->with(['author'])->get();
                $q->with(['records'])->get();
            }])->where(['user_id' => $user_id, 'payment_status' => 'success'])->orderBy('updated_at', 'desc')->get();

            foreach ($purchased_books as $purchased_book) {
                $times = [];
                   foreach($purchased_book->book->records as $book_record){

                       $times[] = $book_record->book_record_duration;
                   }
                $time_sum = BookCategoriesController::AddPlayTime($times);
                $purchased_book->book->mp3_duration = $time_sum;
            }


            return response()->json([
                'status' => 'success',
                'data' => $purchased_books
            ]);
        }

        return response()->json([
            'status' => 'error'
        ],400);
    }

    /**
     * method UserFavoriteBooksAPI
     *
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function UserFavoriteBooksAPI($user_id)
    {

        if (!empty($user_id)) {

            $favorite_books = UserWishList::with(['book' => function ($q) {
                $q->with(['author'])->get();
                $q->with(['records'])->get();
            }])->where(['user_id' => $user_id])->orderBy('id', 'desc')->get();

            foreach ($favorite_books as $favorite_book) {
                $timSearchAuthores = [];
                foreach($favorite_book->book->records as $book_record){

                    $times[] = $book_record->book_record_duration;
                }
                $time_sum = BookCategoriesController::AddPlayTime($times);
                $favorite_book->book->mp3_duration = $time_sum;
            }

            return response()->json([
                'status' => 'success',
                'data' => $favorite_books
            ]);
        }

        return response()->json([
            'status' => 'error',
            'message' => 'user_id is empty'
        ],400);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function UserFavoriteBooksDestroy(Request $request)
    {


        $data = $request->all();

        $validator = Validator::make($data, [
            'user_id' => 'required',
            'book_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        } else {

            $book_id = $request['book_id'];
            $user_id = $request['user_id'];

            $book = UserWishList::where(['user_id' => $user_id, 'book_id' => $book_id])->first();

            $status = 'error';

            if (!is_null($book)) {
                $book->delete();
                $status = 'success';

                return response()->json([
                    'status' => $status
                ]);
            }
            return response()->json([
                'status' => 'error'
            ],400);

        }


    }

    /**
     * method AddWishList
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function AddWishList(Request $request)
    {

        $data = $request->all();

        $validator = Validator::make($data, [
            'user_id' => 'required',
            'book_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        } else {

            $book_id = $request['book_id'];
            $user_id = $request['user_id'];

            if (!is_null(Books::where(['book_id' => $book_id])->first())) {

                $check_book = UserWishList::where([
                    'book_id' => $book_id,
                    'user_id' => $user_id
                ])->first();

                if (is_null($check_book)) {
                    UserWishList::create([
                        'book_id' => $book_id,
                        'user_id' => $user_id
                    ]);
                }

                return response()->json([
                    'status' => 'success'
                ]);
            }
        }

        return response()->json([
            'status' => 'error'
        ],400);

    }

    /**
     * method PageContentAPI
     *
     * @param $page_name
     * @return \Illuminate\Http\JsonResponse
     */
    public function PageContentAPI($page_name)
    {

        if (!empty($page_name)) {
            $page = Pages::where(['sku' => $page_name])->first();

            return response()->json([
                'status' => 'success',
                'data' => $page
            ]);
        }

        return response()->json([
            'status' => 'error'
        ],400);
    }

    /**
     * method SearchBookName
     *
     * @param $search_keyword
     * @return \Illuminate\Http\JsonResponse
     */
    public function SearchBook(Request $request)
    {


        $search_keyword = $request->search_keyword;

        if (!empty($search_keyword)) {

            $books = DB::table('books')
                ->select('book_id','book_title', 'book_description', 'book_average_rating', 'book_image', 'book_href', 'author_name','href','author_image')
                ->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
                ->where('books.book_title', 'like','%'. $search_keyword  . '%')
                ->orWhere('books.book_href', 'like','%'. $search_keyword . '%')
                ->get();

            return response()->json([
                'status' => 'success',
                'data' => $books
            ]);
        }

        return response()->json([
            'status' => 'error'
        ],400);
    }

    /**
     * method Author
     *
     * @param $search_keyword
     * @return \Illuminate\Http\JsonResponse
     */
    public function SearchAuthor(Request $request)
    {

        $search_keyword = $request->search_keyword;
//        dd($request->search_keyword);

        if (!empty($search_keyword)) {
            $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
            );
            $authors = DB::table('authors')
                ->where('author_name', 'like','%'. $search_keyword  . '%')
                ->orWhere('href', 'like','%'. $search_keyword . '%')
                ->get();

            return response()->json([
                'status' => 'success',
                'data' => $authors
            ], 200,$header,JSON_UNESCAPED_UNICODE)->header('Content-Type', 'application/json');;
        }

        return response()->json([
            'status' => 'error'
        ],400);
    }

    /**
     *
     */
    public function getRegistration()
    {
        $countries = json_decode(file_get_contents(public_path('countries.json')));


        return response()->json([
            'status' => 'success',
            'data' => $countries
        ], 200);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function postRegistration(Request $request)
    {

        $data = $request->all();

        $validator = Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password_confirmation' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'gender' => 'required',
            'country' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()],400);
        } else {


            $data['password'] = $request['password'];

            $data['subject'] = 'Welcome to Audiobook';
            $data['from-email'] = 'info@audiobook.am';

            Mail::send('mail-templates.register', ['data' => $data], function ($m) use ($data) {
                $m->from($data['from-email'], 'Audiobook.am');
                $m->to($data['email'])->subject($data['subject']);
            });

            $mail_send = 1;
            if (count(Mail::failures()) > 0) {
                $mail_send = 0;
            }

            User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'telephone' => @$data['telephone'],
                'country' => $data['country'],
                'gender' => $data['gender'],
                'password' => bcrypt($data['password']),
                'mail_send' => $mail_send
            ]);

            return response()->json([
                'status' => 'success'
            ], 200);

        }
    }

    /**
     * @param $book_record_title
     * @param $user_id
     * @return mixed
     */
    public function BookRecord($book_record_title,$user_id){

        $book_id = \App\BookRecords::where(['book_record_title'=>$book_record_title])->pluck('book_id')->first();

        if(!is_null(\App\BookBilling::where(['book_id'=>$book_id,'user_id'=>$user_id])->first())){

            $book_record = \App\BookRecords::where(['book_record_title'=>$book_record_title])->pluck('book_record')->first();

            $path = storage_path() . '/uploads/tmp/'.$book_record.'.mp3';

            $file = \Illuminate\Support\Facades\File::get($path);


            $type = \Illuminate\Support\Facades\File::mimeType($path);

            $response = \Illuminate\Support\Facades\Response::make($file, 200);
//            $response->header("Content-Type", $type);

            return response()->download($path);

        }
    }


}

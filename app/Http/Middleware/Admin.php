<?php


namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class AfterMiddleware
 * @package App\Http\Middleware
 */
class Admin
{
    public function handle($request, Closure $next)
    {

//       if(!Auth::check()){
//
//           return redirect('404');
//       }

       if(Auth::user()->id != 5 or !Auth::check()){
           return redirect('404');

       }


        // Perform action

        return  $next($request);
    }
}
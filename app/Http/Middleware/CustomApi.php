<?php

namespace App\Http\Middleware;

use App\AuthApi;
use Closure;
use Illuminate\Support\Facades\Auth;

class CustomApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty($request->header('clientid')) && !empty($request->header('clientsecret'))){

            if(!is_null(AuthApi::where(['client_id'=>$request->header('clientid'),'client_secret'=>$request->header('clientsecret')])->first())){
                return $next($request);
            }
        }

        return response()->json([
            'status'=>'error',
            'message'=>'authorization failed'
        ],401);
    }
}

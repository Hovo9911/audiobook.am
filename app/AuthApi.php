<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Authors
 * @package App
 */
class AuthApi extends Model
{
    protected $table = 'api_auth';
}
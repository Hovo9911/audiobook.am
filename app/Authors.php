<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Authors
 * @package App
 */
class Authors extends Model
{
    /**
     * @return mixed
     */
   public static function lovely_authors(){
    	return    $lovely_authors   = DB::table('authors')->orderByRaw("RAND()")->limit('3')->get();                                 
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public  function books(){
        return $this->hasMany(Books::class,'author_id','auth_id');
    }
}
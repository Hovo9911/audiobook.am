<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Authors
 * @package App
 */
class Pages extends Model
{
   public $table = 'pages';
}
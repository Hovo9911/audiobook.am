<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWishList
 * @package App
 */
class UserWishList extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_wish_list';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'book_id','user_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book()
    {
        return $this->belongsTo('App\Books','book_id','book_id');
    }




}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWishList
 * @package App
 */
class BookRecords extends Model
{
    /**
     * @var string
     */
    protected $table = 'books_records';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'book_id','book_record_title','book_record_duration','book_record','added_date'
    ];





}

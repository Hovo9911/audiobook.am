<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWishList
 * @package App
 */
class BookBilling extends Model
{
    /**
     * @var string
     */
    protected $table = 'book_billing';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'book_id','payment_status','user_id','payment_token','payment_type','payment_id','order_id','price'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book()
    {
        return $this->belongsTo('App\Books','book_id','book_id');
    }

    public function buyer()
    {
        return $this->belongsTo('App\User','user_id','id');
    }


}

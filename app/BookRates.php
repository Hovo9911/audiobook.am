<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWishList
 * @package App
 */
class BookRates extends Model
{
    /**
     * @var string
     */
    protected $table = 'book_rates';
    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'book_id','rate','user_id'
    ];






}

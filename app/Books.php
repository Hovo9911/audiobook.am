<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Books
 * @package App
 */
class Books extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'book_id','author_id','book_category','book_price','book_title','book_description','book_image','book_href','book_language','slideshow',
    ];

    /**
     * @return mixed
     */
    public static function lovely_books(){
    	return $lovely_books     = DB::table('books')->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
                                              ->orderByRaw("RAND()")->limit('3')->get();                                     
    }

    /**
     * @return mixed
     */
    public static function slideshow_books(){
    	return $slideshow_books  = DB::table('books')->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
                                              ->where('books.slideshow','=',1)->get();                                  
    }

    /**
     * @return mixed
     */
    public static function slideshow_new_added_books(){

    	return $slideshow_new_added_books  = DB::table('books')->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
                                              ->orderBy('books.book_id', 'desc')->take(5)->get();                                    
    }

    /**
     * @return mixed
     */
    public static function slideshow_most_listened_books(){
    	return $slideshow_new_added_books  = DB::table('books')->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
                                              ->orderByRaw("RAND()")->limit('5')->get();                                   
    }

    /**
     * @return mixed
     */
    public static function slideshow_top_rated_books(){
    	return $slideshow_new_added_books  = DB::table('books')->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
                                              ->orderBy("book_average_rating",'DESC')->limit('5')->get();
    }

    /**
     * @return mixed
     */
    public static function new_added_books(){
     
      return DB::table('books')->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
                               ->orderByRaw('books.book_id DESC')
                               ->paginate(12);
    }

    /**
     * @return mixed
     */
    public static function most_listened_books(){
     
      return DB::table('books')->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
                               ->orderByRaw("RAND()")
                               ->paginate(12);
    }

    /**
     * @return mixed
     */
    public static function top_rated_books(){
     
      return DB::table('books')->leftJoin('authors', 'authors.auth_id', '=', 'books.author_id')
          ->orderBy("book_average_rating",'DESC')
                               ->paginate(12);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\Authors','author_id','auth_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rate()
    {
        return $this->hasMany('App\BookRates','book_id','book_id')->selectRaw('ROUND(avg(rate)) as rating,book_id')->groupBy('book_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function records()
    {
        return $this->hasMany('App\BookRecords','book_id','book_id');
    }


}




 
